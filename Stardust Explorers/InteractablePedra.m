//
//  InteractablePedra.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 03/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "InteractablePedra.h"

@implementation InteractablePedra
-(InteractablePedra *)init{
    self =[super init];
    if (self){
        destruido = false;
        
    }
    return self;
    
}
-(void) destruirPedra{
    if (destruido == false) {
        destruido = true;
    }
}
@synthesize destruido;
@end
