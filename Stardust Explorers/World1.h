//
//  MyScene.h
//  tiletest
//

//  Copyright (c) 2014 super. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "JSTileMap.h"
#import "Joystick.h"
#import "PlayerCyberPala.h"
#import "PlayerEngIdol.h"
#import "PlayerHacker.h"
#import "PlayerMateMago.h"
#import "InteractableHack.h"
#import "WorldBase.h"

@interface World1 : WorldBase{
   
}


@property (nonatomic, strong) JSTileMap *map;
@property (strong) TMXLayer *meta;
@property (nonatomic, strong) TMXLayer *walls;
@property (nonatomic, assign) CGPoint desiredPosition;




@end
