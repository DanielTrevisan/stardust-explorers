//
//  InimigoBase.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 09/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface InimigoBase : SKSpriteNode{
    int pontdevidamaxima;
    int pontdevidaatual;
    NSString *estado;
    
}

-(int)tomarDano:(int) dano;
-(void)ataqueBasico;
-(void)movimento;

@property int pontdevidamaxima;
@property int pontdevidaatual;
@property NSString *estado;

@end
