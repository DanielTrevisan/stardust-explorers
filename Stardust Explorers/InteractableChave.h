//
//  InteractableChave.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 04/11/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//


#import "ObjectInteractable.h"

@interface InteractableChave : ObjectInteractable{
    Boolean ativado;
}

-(void) begin;

@property Boolean ativado;

@end
