//
//  InteractablePedra.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 03/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "ObjectInteractable.h"

@interface InteractablePedra : ObjectInteractable{
    bool destruido;
    NSString *nomegrafico;
}
-(InteractablePedra *)init;
-(void)destruirPedra;

@property bool destruido;

@end
