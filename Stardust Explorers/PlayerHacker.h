//
//  PlayerHacker.h
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "Player.h"

@interface PlayerHacker : Player
-(void)cooldownHack1:(NSTimeInterval)tempo;
-(void)cooldownHack2:(NSTimeInterval)tempo;

@end
