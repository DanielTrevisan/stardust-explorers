//
//  VotingView.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 03/11/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "VotingView.h"
#import "World1.h"
#import "WorldTeste.h"

typedef NS_ENUM(NSUInteger, GameState) {
    kGameStateWaitingForMatch = 0,
    kGameStateWaitingForAllSelected, //Character Selection
    kGameStateWaitingForStartStage, //Todos escolheram seus personagens
    kGameStateWaitingForStageSelect, //Todos estão na tela de escolha de fase/ upgrades
    kGameStateWaitingForGameStart, //A fase sorteada é mostrada
    kGameStateActive, //Jogando
    kGameStateAcabando, // Um dos jogadores chegou
    kGameStateDone
};
#pragma mark Mensagens
typedef NS_ENUM(NSUInteger, MessageType) {
    kMessageTypeSelectedClass, //Escolheu a classe, deve deixar indisponivel aos outros
    kMessageTypeStageProgress, //Grava a progressão de fase e mostra a menos avançada
    kMessageTypeStageSelectBegin,  //O ultimo a escolher a classe manda essa mensagem para os outros
    kMessageTypeStageSelected, //Manda o stage escolhido para os outros
    kMessageTypeStageSorteado, //O ultimo a escolher o stage envia aos outros que está pronto o sorteio
    kMessageTypeMove, //Acao de movimento dos personagens
    kMessageTypeSkill, //Habilidades dos personagens
    kMessageTypeChangeClass, //Troca de personagens quando players = 2 ou 3
    kMessageTypeGameOver,
    kMessageTypeStageEnd
};

typedef struct {
    MessageType messageType;
} Message;

typedef struct {
    Message message;
    ClassName nomeC;//0=Paladino 1=Idol 2= Hacker 3=Mago
    ClassName lastClass;
} MessageSelectedClass;

typedef struct {
    Message message;
    int stagescompleted;
} MessageStageProgress;

typedef struct{
    Message message;
}MessageStageSelectBegin;

typedef struct{
    Message message;
    int numstage;
}MessageStageSelected;

typedef struct{
    Message message;
    int numstagesorteado;
}MessageStageSorteado;

typedef struct{
    Message message;
    CGPoint posicao;
    char direction;
}MessageMove;

typedef struct{
    Message message;
    int numClasse;
    int numSkill; //0 = básico; 1= Habilidade 1 2= Habilidade 2
}MessageSkill;

typedef struct{
    Message message;
    ClassName nomeC;
}MessageChangeClass;


typedef struct{
    Message message;
}MessageGameOver;

typedef struct{
    Message message;
}MessageStageEnd;

@implementation VotingView
-(id)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]) {
        outrosVotos = [outrosVotos initWithCapacity:4];
        _localVote = (NSUInteger)[self.userData objectForKey:@"localSelection"];
        [self mostraSelecoes];
        [self enviaFase];
    }
    return self;
}
-(void)enviaFase{
    MessageStageSelected stageselected;
    stageselected.numstage = _localVote;
    NSData *data =[NSData dataWithBytes:&stageselected length:sizeof(MessageStageSelected)];
    [super sendData:data];

}
-(void)mostraSelecoes{
    stg1 = [SKSpriteNode spriteNodeWithImageNamed:@"chave"];
    stg1.position = CGPointMake(self.size.width*1/4, self.size.height*1/2);
}
- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID {
    Message *message = (Message*)[data bytes];
    if(message ->messageType == kMessageTypeStageSelected){
        MessageStageSelected *stageselected = (MessageStageSelected *)[data bytes];
        NSNumber *outrosel = [ NSNumber numberWithInt:stageselected->numstage];
        [outrosVotos addObject:outrosel];
        if ([outrosVotos count] == [super.matchBase.playerIDs count] ) {
            if (super.ultimaclasse == kClassPaladin ||(super.ultimaclasse == kClassIdol && palaSelected ==false) ||(super.ultimaclasse == kClassHacker && palaSelected == false && idolSelected == false)) {
                int num = arc4random_uniform([super.matchBase.playerIDs count]+1);
                switch (num) {
                    case 1:
                        stagesorteado = _localVote;
                        break;
                    case 2:
                        stagesorteado = [[outrosVotos objectAtIndex:0] integerValue];
                    case 3:
                        stagesorteado = [[outrosVotos objectAtIndex:1] integerValue];
                    case 4:
                        stagesorteado = [[outrosVotos objectAtIndex:2] integerValue];
                    default:
                        NSLog(@"erro de logica do programador");
                        break;
                }
            }
            MessageStageSorteado stgsorteio;
            stgsorteio.numstagesorteado = stagesorteado;
            NSData *data =[NSData dataWithBytes:&stgsorteio length:sizeof(MessageStageSorteado)];
            [super sendData:data];
            [self mudaFaseWithNumber:stgsorteio.numstagesorteado];
            

        }
        if (message->messageType == kMessageTypeStageSorteado) {
            MessageStageSorteado *stgsorteado = (MessageStageSorteado *)[data bytes];
            stagesorteado = stgsorteado->numstagesorteado;
            [self mudaFaseWithNumber:stagesorteado];
        }
    }
}

-(void)mudaFaseWithNumber:(int) fase{
    switch (fase) {
        case 1:{
            SKView *skView = (SKView *)self.view;
            SKScene *scene = [World1 sceneWithSize:skView.bounds.size];
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
            NSLog(@"Fase 1");
            break;
        }
        case 2:{
            SKView * skView = (SKView *)self.view;
            SKScene *scene = [WorldTeste sceneWithSize:skView.bounds.size];
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
            NSLog(@"Fase 2");
            break;
        }
            
            
        case 3:
            
            break;
    }
}
@end
