//
//  SelecaoDeFases1.m
//  Stardust Explorers
//
//  Created by Sergio Mauwad Golbert on 9/3/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "SelecaoDeFases1.h"
#import "World1.h"
#import "World1f3.h"
#import "WorldView.h"
#import "WorldTeste.h"
#import "GameData.h"
#import "GameKitHelper.h"

@implementation SelecaoDeFases1

-(id)initWithSize:(CGSize)size {

    if (self = [super initWithSize:size]) {
        SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"space1.jpg"];
        bgImage.position = CGPointMake(self.size.width/2,self.size.height/2);
        [self addChild:bgImage];
        SKSpriteNode *World1pla;
        World1pla = [SKSpriteNode spriteNodeWithImageNamed:@"Office_Planet"];
        World1pla.position = CGPointMake(self.size.width/2, (self.size.height/2));
        [self addChild:World1pla];
        SKAction *scaleup= [SKAction scaleBy:3 duration:1];
        [World1pla runAction:scaleup completion:^{
            fase1 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
            fase1.position = CGPointMake(self.size.width/2, self.size.height*7/8);
            [self addChild:fase1];
            
            SKLabelNode *f1= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            f1.text = @"Fase 1";
            f1.position = CGPointMake(self.size.width/2 + 2* fase1.size.width, self.size.height*7/8);
            [self addChild:f1];
            
            fase2 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
            fase2.position = CGPointMake(self.size.width/2, self.size.height*6/8);
            
            [self addChild:fase2];
            SKLabelNode *f2= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            f2.text = @"Fase 2";
            if (([GameData sharedGameData].stagesCompleted >=1 && [GameKitHelper sharedGameKitHelper].enableGameCenter == NO )||
                ([GameData sharedGameData].stageMult >=1 && [GameKitHelper sharedGameKitHelper].enableGameCenter == YES)) {
                f2.fontColor = [UIColor whiteColor];
            }
            else{
                
                f2.fontColor = [UIColor grayColor];
                
            }
            f2.position = CGPointMake(self.size.width/2 + 2* fase2.size.width, self.size.height*6/8);
            [self addChild:f2];
            
            fase3 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
            fase3.position = CGPointMake(self.size.width/2, self.size.height*5/8);
            [self addChild:fase3];
            SKLabelNode *f3= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            f3.text = @"Fase 3";
            if (([GameData sharedGameData].stagesCompleted >=2 && [GameKitHelper sharedGameKitHelper].enableGameCenter == NO )||
                ([GameData sharedGameData].stageMult >=2 && [GameKitHelper sharedGameKitHelper].enableGameCenter == YES)) {
                f3.fontColor = [UIColor whiteColor];
            }
            else{
                f3.fontColor = [UIColor grayColor];
                
            }
            
            f3.position = CGPointMake(self.size.width/2 + 2* fase2.size.width, self.size.height*5/8);
            [self addChild:f3];
            
            fase4 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
            fase4.position = CGPointMake(self.size.width/2, self.size.height*4/8);
            [self addChild:fase4];
            SKLabelNode *f4= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            f4.text = @"Fase 4";
            if (([GameData sharedGameData].stagesCompleted >=3 && [GameKitHelper sharedGameKitHelper].enableGameCenter == NO )||
                ([GameData sharedGameData].stageMult >=3 && [GameKitHelper sharedGameKitHelper].enableGameCenter == YES)) {
                f4.fontColor = [UIColor whiteColor];
            }
            else{
                f4.fontColor = [UIColor grayColor];
                
            }
            
            f4.position = CGPointMake(self.size.width/2 + 2* fase2.size.width, self.size.height*4/8);
            [self addChild:f4];
            
            fase5 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
            fase5.position = CGPointMake(self.size.width/2, self.size.height*3/8);
            [self addChild:fase5];
            SKLabelNode *f5= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            f5.text = @"Fase 5";
            if (([GameData sharedGameData].stagesCompleted >=4 && [GameKitHelper sharedGameKitHelper].enableGameCenter == NO)||
                ([GameData sharedGameData].stageMult >=4 && [GameKitHelper sharedGameKitHelper].enableGameCenter == YES)) {
                f5.fontColor = [UIColor whiteColor];
            }
            else{
                f5.fontColor = [UIColor grayColor];
                
            }
            
            f5.position = CGPointMake(self.size.width/2 + 2* fase2.size.width, self.size.height*3/8);
            [self addChild:f5];
            
            fase6 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
            fase6.position = CGPointMake(self.size.width/2, self.size.height*2/8);
            [self addChild:fase6];
            SKLabelNode *f6= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            f6.text = @"Fase 6";
            if (([GameData sharedGameData].stagesCompleted >=5 && [GameKitHelper sharedGameKitHelper].enableGameCenter == NO)||
                ([GameData sharedGameData].stageMult >=5 && [GameKitHelper sharedGameKitHelper].enableGameCenter == YES)) {
                f6.fontColor = [UIColor whiteColor];
            }
            else{
                f6.fontColor = [UIColor grayColor];
                
            }
            
            f6.position = CGPointMake(self.size.width/2 + 2* fase2.size.width, self.size.height*2/8);
            [self addChild:f6];
            
            back = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
            back.text = @"Voltar";
            back.position = CGPointMake(self.size.width/2, self.size.height*1/8);
            [self addChild:back];
        }];
        
        
        
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        if ([GameKitHelper sharedGameKitHelper].enableGameCenter == NO) {
            if ([fase1 containsPoint:location])
            {
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [World1 sceneWithSize:skView.bounds.size];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
                NSLog(@"Fase 1");
                
                
            }
            if ([fase2 containsPoint:location]&& [GameData sharedGameData].stagesCompleted >=1)
            {
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [WorldTeste sceneWithSize:skView.bounds.size];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
                NSLog(@"Fase 2");
                
            }
            if ([fase3 containsPoint:location]&& [GameData sharedGameData].stagesCompleted >=2)
            {
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [World1f3 sceneWithSize:skView.bounds.size];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
                NSLog(@"Fase 3");
            }
            if ([fase4 containsPoint:location]&& [GameData sharedGameData].stagesCompleted >=3)
            {
                NSLog(@"Fase 4");
            }
            if ([fase5 containsPoint:location]&& [GameData sharedGameData].stagesCompleted >=4)
            {
                NSLog(@"Fase 5");
            }
            if ([fase6 containsPoint:location]&& [GameData sharedGameData].stagesCompleted >=5)
            {
                NSLog(@"Fase 6");
            }
            
        }
        if ([GameKitHelper sharedGameKitHelper].enableGameCenter == YES) {
            if ([fase1 containsPoint:location])
            {
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [World1 sceneWithSize:skView.bounds.size];
                NSNumber *numfase = [NSNumber numberWithInt:1];
                [scene.userData setObject:numfase forKey:@"localSelection"];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
                NSLog(@"Fase 1");
                
                
            }
            if ([fase2 containsPoint:location]&& [GameData sharedGameData].stageMult >1)
            {
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [WorldTeste sceneWithSize:skView.bounds.size];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
                NSLog(@"Fase 2");
                
            }
            if ([fase3 containsPoint:location])
            {
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [World1f3 sceneWithSize:skView.bounds.size];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
                NSLog(@"Fase 3");
            }
            if ([fase4 containsPoint:location])
            {
                NSLog(@"Fase 4");
            }
            if ([fase5 containsPoint:location])
            {
                NSLog(@"Fase 5");
            }
            if ([fase6 containsPoint:location])
            {
                NSLog(@"Fase 6");
            }
            
        }
        if([back containsPoint:location]){
            SKView * skView = (SKView *)self.view;
            SKScene *sceneback = [WorldView sceneWithSize:skView.bounds.size];
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:sceneback transition:sceneTransition];
            NSLog(@"Fase 1");
            
        }
    }
}




@end
