//
//  PlayerHacker.m
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "PlayerHacker.h"

@implementation PlayerHacker
+ (PlayerHacker *)defaultStore
{
    static PlayerHacker *defaultStore = nil;
    if(!defaultStore)
        defaultStore = [[super allocWithZone:nil] init];
    
    return defaultStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultStore];
}
-(PlayerHacker *) init{
    self = [super init];
    if (self){
        spritefiles= @"koalio_stand.png";
        playerdirection = 'd';

        estado = @"pronto";
        estadoHacker1 = @"ataque";
        estadoHacker2 = @"ataque";
        pontdevidaatual = 4;
        pontdevidamax = 4;
        
    }
    return self;
}

-(void)cooldownHacker1:(NSTimeInterval)tempo{
    //TRUE = não pode usar habilidade
    //FALSE = Pode usar habilidade
    estadoHacker1 = @"cooldown";
    SKAction *wait =[SKAction waitForDuration:tempo];
    [self runAction:wait completion:^ {
        estadoHacker1 = @"ataque";
    }];
}
-(void)cooldownHacker2:(NSTimeInterval)tempo{
    //TRUE = não pode usar habilidade
    //FALSE = Pode usar habilidade
    estadoHacker2 = @"cooldown";
    SKAction *wait =[SKAction waitForDuration:tempo];
    [self runAction:wait completion:^ {
        estadoHacker2 = @"ataque";
    }];
}


@end
