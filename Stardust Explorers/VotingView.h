//
//  VotingView.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 03/11/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MultiBase.h"

@interface VotingView : MultiBase{
    int stagesorteado;
    SKSpriteNode *stg1;
    SKSpriteNode *stg2;
    SKSpriteNode *stg3;
    SKSpriteNode *stg4;
}

@property NSUInteger localVote;

@end
