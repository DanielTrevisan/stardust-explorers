//
//  MultiBase.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 07/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "MultiBase.h"
#import "PlayerCyberPala.h"
#import "PlayerEngIdol.h"
#import "SKTUtils.h"
#import "GameKitHelper.h"
#import "MutiViewController.h"
#import "ViewController.h"
#import "WorldView.h"
#import "MenuView.h"
#import "GameData.h"

#pragma mark Game States

typedef NS_ENUM(NSUInteger, GameState) {
    kGameStateWaitingForMatch = 0,
    kGameStateWaitingForAllSelected, //Character Selection
    kGameStateWaitingForStartStage, //Todos escolheram seus personagens
    kGameStateWaitingForStageSelect, //Todos estão na tela de escolha de fase/ upgrades
    kGameStateWaitingForGameStart, //A fase sorteada é mostrada
    kGameStateActive, //Jogando
    kGameStateAcabando, // Um dos jogadores chegou
    kGameStateDone
};
#pragma mark Mensagens
typedef NS_ENUM(NSUInteger, MessageType) {
    kMessageTypeSelectedClass, //Escolheu a classe, deve deixar indisponivel aos outros
    kMessageTypeStageProgress, //Grava a progressão de fase e mostra a menos avançada
    kMessageTypeStageSelectBegin,  //O ultimo a escolher a classe manda essa mensagem para os outros
    kMessageTypeStageSelected, //Manda o stage escolhido para os outros
    kMessageTypeStageSorteado, //O ultimo a escolher o stage envia aos outros que está pronto o sorteio
    kMessageTypeMove, //Acao de movimento dos personagens
    kMessageTypeSkill, //Habilidades dos personagens
    kMessageTypeChangeClass, //Troca de personagens quando players = 2 ou 3
    kMessageTypeGameOver,
    kMessageTypeStageEnd
};

typedef struct {
    MessageType messageType;
} Message;

typedef struct {
    Message message;
    ClassName nomeC;//0=Paladino 1=Idol 2= Hacker 3=Mago
    ClassName lastClass;
} MessageSelectedClass;

typedef struct {
    Message message;
    int stagescompleted;
} MessageStageProgress;

typedef struct{
    Message message;
}MessageStageSelectBegin;

typedef struct{
    Message message;
    int numstage;
}MessageStageSelected;

typedef struct{
    Message message;
    int numstagesorteado;
}MessageStageSorteado;

typedef struct{
    Message message;
    CGPoint posicao;
    char direction;
}MessageMove;

typedef struct{
    Message message;
    int numClasse;
    int numSkill; //0 = básico; 1= Habilidade 1 2= Habilidade 2
}MessageSkill;

typedef struct{
    Message message;
    ClassName nomeC;
}MessageChangeClass;


typedef struct{
    Message message;
}MessageGameOver;

typedef struct{
    Message message;
}MessageStageEnd;

@interface MultiBase()<GKGameCenterControllerDelegate, GKMatchmakerViewControllerDelegate, GKMatchDelegate>

@end


@implementation MultiBase {
    NSMutableArray *_playerprogress;
    NSUInteger _currentPlayerIndex;
    GameState _gamestate;
    ClassName ultimaclasse;  //0=Paladino 1=Idol 2= Hacker 3=Mago
    int quantselected;
}
- (void)viewDidAppear:(BOOL)animated {
    [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];

}
+ (instancetype)sharedGameKitHelper
{
    static MultiBase *sharedMultiBase;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMultiBase = [[MultiBase alloc] init];
    });
    return sharedMultiBase;
}
-(id)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]) {
        if([GameKitHelper sharedGameKitHelper].enableGameCenter ==YES){
        quantselected =0;
        [self playerAuthenticated];
        stagesPlayers = [stagesPlayers initWithCapacity:4];
        [self addCharas]; // chamar essa funcao depois de encontrar a partida do matchmaking
        }
    }
    return self;
}
-(void)addCharas{
    
    selectIdol = [SKSpriteNode spriteNodeWithImageNamed:@"charselectIdol"];
    selectIdol.position = CGPointMake(0, 0);
    selectIdol.anchorPoint = CGPointMake(0, 0);
    [self addChild:selectIdol];
    
    selectHack = [SKSpriteNode spriteNodeWithImageNamed:@"charselectHack"];
    selectHack.position = CGPointMake(self.size.width*1/4, 0);
    selectHack.anchorPoint = CGPointMake(0, 0);
    [self addChild:selectHack];
    selectPala = [SKSpriteNode spriteNodeWithImageNamed:@"charselectPala"];
    selectPala.position = CGPointMake(self.size.width*2/4, 0);
    selectPala.anchorPoint = CGPointMake(0, 0);
    [self addChild:selectPala];

    selectMago = [SKSpriteNode spriteNodeWithImageNamed:@"charselectMage"];
    selectMago.position = CGPointMake(self.size.width*3/4, 0);
    selectMago.anchorPoint = CGPointMake(0, 0);
    [self addChild:selectMago];
    
    
}
-(void)showGameCenter{
GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController
                                                     alloc] init];
if (gameCenterController != nil)
{
    gameCenterController.gameCenterDelegate = self;
    [self.view.window.rootViewController presentViewController: gameCenterController animated: YES
                     completion:nil];
}
}
-(void)playerAuthenticated{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController
                                                         alloc] init];
    if (gameCenterController != nil)
    {
        [self.view.window.rootViewController presentViewController: gameCenterController animated: YES
                         completion:nil];
    }
    NSString* title = @"MULTIJOGADORES";
    NSString* message = @"eae";
    [GKNotificationBanner showBannerWithTitle: title message: message
                            completionHandler:^{
                                NSLog(@"teste mensagem end");
                                [self hostMatch];

                              }];

}
- (void)hostMatch
{
    matchStarted = NO;

    _request = [[GKMatchRequest alloc] init];
    _request.minPlayers = 2;
    _request.maxPlayers = 4;
    GKMatchmakerViewController *mmvc = [[GKMatchmakerViewController alloc]
                                        initWithMatchRequest:_request];
    mmvc.matchmakerDelegate = self;
    [self.view.window.rootViewController presentViewController:mmvc animated:YES completion:nil];
}
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController
                                           *)gameCenterViewController
{
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)matchmakerViewControllerWasCancelled:(GKMatchmakerViewController
                                              *)viewController
{
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    SKView * skView = (SKView *)self.view;
    SKScene *scene = [MenuView sceneWithSize:skView.bounds.size];
    SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
    [self.view presentScene:scene transition:sceneTransition];

}
- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFailWithError:(NSError *)error
{
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFindMatch:(GKMatch *)match
{
    [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    self.matchBase = match; // Use a retaining property to retain the match.
    match.delegate = self;
    if (!matchStarted && match.expectedPlayerCount == 0)
    {
        matchStarted = YES;
        // Insert game-specific code to start the match.
    }
}
#pragma mark SELECAO DE CLASSES
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        if ([start containsPoint:location]) {
            MessageStageProgress *stgprogmsg;
            stgprogmsg->message.messageType = kMessageTypeStageProgress;
            stgprogmsg->stagescompleted = [GameData sharedGameData].stagesCompleted;
            NSData *data =[NSData dataWithBytes:&stgprogmsg length:sizeof(MessageStageProgress)];
            [self sendData:data];
            
           
            
        }
        if ([selectIdol containsPoint:location] && idolSelected == false && ![start containsPoint:location] && matchStarted ==YES){
            MessageSelectedClass mensagem;
            mensagem.message.messageType = kMessageTypeChangeClass;
            mensagem.nomeC=kClassIdol;
            
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];
            idolSelected = true;
            if (ultimaclasse == kClassHacker) {
                quantselected--;
                hackSelected = false;
                mensagem.lastClass = kClassHacker;
                [selectHack runAction:normal];

            }
            if (ultimaclasse == kClassMage) {
                quantselected--;

                magoSelected = false;
                mensagem.lastClass = kClassMage;

                [selectMago runAction:normal];
            }
            if (ultimaclasse == kClassPaladin) {
                quantselected--;

                palaSelected = false;
                mensagem.lastClass = kClassPaladin;
                [selectPala runAction:normal];
            }
            ultimaclasse = kClassIdol;
            [selectIdol runAction:graytint];
            quantselected++;
            NSData *data =[NSData dataWithBytes:&mensagem length:sizeof(MessageSelectedClass)];
            [self sendData:data];
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }


        }
        if ([selectHack containsPoint:location] && hackSelected == false && ![start containsPoint:location]&& matchStarted ==YES) {
            MessageSelectedClass mensagem;
            mensagem.message.messageType = kMessageTypeChangeClass;
            mensagem.nomeC=kClassHacker;
            
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];

            hackSelected = true;
            if (ultimaclasse == kClassMage) {
                quantselected--;
                mensagem.lastClass = kClassMage;

                magoSelected = false;
                [selectMago runAction:normal];
            }
            if (ultimaclasse == kClassPaladin) {
                quantselected--;
                mensagem.lastClass = kClassPaladin;

                palaSelected = false;
                [selectPala runAction:normal];
            }
            if (ultimaclasse == kClassIdol) {
                quantselected--;
                mensagem.lastClass = kClassIdol;

                idolSelected = false;
                [selectIdol runAction:normal];
            }
            ultimaclasse = kClassHacker;
            NSData *data =[NSData dataWithBytes:&mensagem length:sizeof(MessageSelectedClass)];
            [self sendData:data];
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            [selectHack runAction:graytint];
            quantselected++;

            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
            
            


        }
        if ([selectMago containsPoint:location] && magoSelected == false && ![start containsPoint:location]&& matchStarted ==YES) {
            MessageSelectedClass mensagem;
            mensagem.message.messageType = kMessageTypeChangeClass;
            mensagem.nomeC=kClassMage;
            
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];

            magoSelected = true;
            if (ultimaclasse == kClassPaladin) {
                quantselected--;
                mensagem.lastClass = kClassPaladin;

                palaSelected = false;
                [selectPala runAction:normal];
            }
            if (ultimaclasse == kClassIdol) {
                quantselected--;
                mensagem.lastClass = kClassIdol;

                idolSelected = false;
                [selectIdol runAction:normal];
            }
            if (ultimaclasse == kClassHacker) {
                quantselected--;
                mensagem.lastClass = kClassHacker;

                hackSelected = false;
                [selectHack runAction:normal];
                
            }
            ultimaclasse = kClassMage;
            NSData *data =[NSData dataWithBytes:&mensagem length:sizeof(MessageSelectedClass)];
            [self sendData:data];
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            [selectMago runAction:graytint];
            quantselected++;
            
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
           



        }
        if ([selectPala containsPoint:location] && palaSelected == false && ![start containsPoint:location]&& matchStarted ==YES) {
            MessageSelectedClass mensagem;
            mensagem.message.messageType = kMessageTypeChangeClass;
            mensagem.nomeC=kClassPaladin;
            
            palaSelected = true;
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];

            if (ultimaclasse == kClassIdol) {
                quantselected--;
                mensagem.lastClass = kClassIdol;

                idolSelected = false;
                [selectIdol runAction:normal];
            }
            if (ultimaclasse == kClassHacker) {
                quantselected--;
                mensagem.lastClass = kClassHacker;

                hackSelected = false;
                [selectHack runAction:normal];
                
            }
            if (ultimaclasse == kClassMage) {
                quantselected--;
                mensagem.lastClass = kClassMage;

                magoSelected = false;
                [selectMago runAction:normal];
            }
            ultimaclasse = kClassPaladin;
            NSData *data =[NSData dataWithBytes:&mensagem length:sizeof(MessageSelectedClass)];
            [self sendData:data];
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            
            [selectPala runAction:graytint];
            quantselected++;
            
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
           


        }
    }
}
- (void)sendData:(NSData*)data
{
    NSError *error;
  //  GameKitHelper *gameKitHelper = [GameKitHelper sharedGameKitHelper];
    
    BOOL success = [_matchBase
                    sendDataToAllPlayers:data
                    withDataMode:GKMatchSendDataReliable
                    error:&error];
    
    if (!success) {
        NSLog(@"Error sending data:%@", error.localizedDescription);
    }
}
- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID {
    //1
    Message *message = (Message*)[data bytes];
    if (message->messageType == kMessageTypeChangeClass) {
        MessageSelectedClass *messageCC = (MessageSelectedClass*)[data bytes];
        
        NSLog(@"Received class: %d", messageCC->nomeC);
        if (messageCC->nomeC == kClassHacker) {
            quantselected++;
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];
            
            hackSelected = true;
            if (messageCC->lastClass == kClassMage) {
                quantselected--;
                
                magoSelected = false;
                [selectMago runAction:normal];
            }
            if (messageCC->lastClass == kClassPaladin) {
                quantselected--;
                
                palaSelected = false;
                [selectPala runAction:normal];
            }
            if (messageCC->lastClass == kClassIdol) {
                quantselected--;
                
                idolSelected = false;
                [selectIdol runAction:normal];
            }
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            [selectHack runAction:graytint];
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
        }
        else if (messageCC->nomeC == kClassIdol){
            quantselected++;
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];

            idolSelected = true;
            if (messageCC->lastClass == kClassMage) {
                quantselected--;
                
                magoSelected = false;
                [selectMago runAction:normal];
            }
            if (messageCC->lastClass == kClassPaladin) {
                quantselected--;
                
                palaSelected = false;
                [selectPala runAction:normal];
            }
            if (messageCC->lastClass == kClassHacker) {
                quantselected--;
                
                hackSelected = false;
                [selectHack runAction:normal];
            }
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            [selectIdol runAction:graytint];
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
        }
        else if(messageCC->nomeC == kClassMage){
            quantselected++;
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];
            if (messageCC->lastClass == kClassIdol) {
                quantselected--;
                
                idolSelected = false;
                [selectIdol runAction:normal];
            }
            if (messageCC->lastClass == kClassPaladin) {
                quantselected--;
                
                palaSelected = false;
                [selectPala runAction:normal];
            }
            if (messageCC->lastClass == kClassHacker) {
                quantselected--;
                
                hackSelected = false;
                [selectHack runAction:normal];
            }
            magoSelected = true;
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            [selectMago runAction:graytint];
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
        }
        else if (messageCC->nomeC == kClassPaladin){
            quantselected++;
            SKAction *normal = [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15];
            if (messageCC->lastClass == kClassIdol) {
                quantselected--;
                
                idolSelected = false;
                [selectIdol runAction:normal];
            }
            if (messageCC->lastClass == kClassMage) {
                quantselected--;
                
                magoSelected = false;
                [selectMago runAction:normal];
            }
            if (messageCC->lastClass == kClassHacker) {
                quantselected--;
                
                hackSelected = false;
                [selectHack runAction:normal];
            }
            palaSelected = true;
            SKAction *graytint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:1];
            [selectPala runAction:graytint];
            if (quantselected == [_matchBase.playerIDs count]) {
                _gamestate = kGameStateWaitingForStartStage;
                start = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
                start.position = CGPointMake(self.size.width/2, self.size.height/2);
                start.text = @"Selecionar Mundo";
                [self addChild:start];
            }
        }
      
    }
#pragma mark StageProgress
    if(message->messageType == kMessageTypeStageProgress){
        MessageStageProgress *stageprogress = (MessageStageProgress *)[data bytes];
        NSNumber *stagenumbers = [NSNumber numberWithInt:stageprogress->stagescompleted];
        [stagesPlayers addObject:stagenumbers];
        if ([stagesPlayers count] == [_matchBase.playerIDs count] ) {
            int menorProgresso = [GameData sharedGameData].stagesCompleted;
            for (NSNumber *numeros in stagesPlayers) {
                if (numeros.intValue <menorProgresso) {
                    menorProgresso = numeros.intValue;
                }
            }
            [GameData sharedGameData].stageMult = menorProgresso;
            SKView * skView = (SKView *)self.view;
            SKScene *scene = [WorldView sceneWithSize:skView.bounds.size];
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
        }
    }
#pragma mark SorteioStage
    if(message ->messageType == kMessageTypeStageSelected){
        MessageStageSelected *stageselected = (MessageStageSelected *)[data bytes];
        NSNumber *outrosel = [ NSNumber numberWithInt:stageselected->numstage];
        [outrosVotos addObject:outrosel];
        if ([outrosVotos count] == [_matchBase.playerIDs count] ) {
            if (ultimaclasse == kClassPaladin ||(ultimaclasse == kClassIdol && palaSelected ==false) ||(ultimaclasse == kClassHacker && palaSelected == false && idolSelected == false)) {
                ///sorteio
            }
        }
    }
}


@end


