//
//  InteractableHack.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 03/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "ObjectInteractable.h"

@interface InteractableHack : ObjectInteractable{
    Boolean ativado;
}

-(void) ativarHack;
-(void) begin;

@property Boolean ativado;

@end

