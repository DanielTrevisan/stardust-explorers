//
//  main.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 22/08/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
