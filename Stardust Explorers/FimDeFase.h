//
//  FimDeFase.h
//  Stardust Explorers
//
//  Created by Sergio Mauwad Golbert on 9/4/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface FimDeFase : SKScene{
    
    SKLabelNode *next;
    SKLabelNode *lvlselect;
    SKLabelNode *replay;
    SKLabelNode *Upgrades;
}

@end
