//
//  FimDeFase.m
//  Stardust Explorers
//
//  Created by Sergio Mauwad Golbert on 9/4/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "FimDeFase.h"
#import "SelecaoDeFases1.h"
#import "World1.h"
#import "WorldTeste.h"
#import "GameData.h"
#import "UpgradeScene.h"

@implementation FimDeFase

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        SKLabelNode *parabens = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        parabens.text = @"Fase Concluída!";
        parabens.fontSize = 55;
        parabens.position = CGPointMake(self.size.width*1/2, self.size.height*4/5);
        [self addChild:parabens];
        
        next = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        next.text= @"Próxima Fase";
        next.position = CGPointMake(self.size.width*3/4, self.size.height/4);
        
        [self addChild:next];
        lvlselect = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        lvlselect.text= @"Escolha de Fases";
        lvlselect.position = CGPointMake(self.size.width*1/2, self.size.height/4);
        
        [self addChild:lvlselect];
        
        replay = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        replay.text= @"Jogar novamente";
        replay.position = CGPointMake(self.size.width*1/4, self.size.height/4);
        
        [self addChild:replay];
        
        Upgrades = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        Upgrades.text = @"Upgrades";
        Upgrades.position = CGPointMake(self.size.width*1/2, self.size.height/8);
        [self addChild:Upgrades];
        
    }
    return self;
}
-(void)didMoveToView:(SKView *)view{
    NSNumber *pecascol =[self.userData objectForKey:@"numPecas"];
    SKLabelNode *pecascoletadas = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
   // NSNumber *num =[[GameData sharedGameData].pecasColetadas objectAtIndex:1];
    NSLog(@"teste 1 fase :%d pecas",pecascol.intValue);
    NSString *pecasString= [NSString stringWithFormat:@"Pecas coletadas: "];
    for(int i =1;i<=4;i++){
        SKSpriteNode *pecaCol = [SKSpriteNode spriteNodeWithImageNamed:@"peca"];
        pecaCol.position = CGPointMake(self.size.width *4/6+pecaCol.size.width*i-1, self.size.height*3/6);
        SKAction *tint;
        if (pecascol.intValue>=i) {
            
        }
        else{
            tint = [SKAction colorizeWithColor:[UIColor grayColor] colorBlendFactor:1 duration:0.1];
        }
        [pecaCol runAction:tint];
        [self addChild:pecaCol];
       
    }
    pecascoletadas.text = pecasString;
    pecascoletadas.position = CGPointMake(self.size.width *5/6, (self.size.height*3/6)+70);
    [self addChild:pecascoletadas];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if ([next containsPoint:location])
        {
            SKView * skView = (SKView *)self.view;

            switch ([GameData sharedGameData].ultimoStage) {
                case 1:{
                    SKScene *scene = [WorldTeste sceneWithSize:skView.bounds.size];
                    SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                    [self.view presentScene:scene transition:sceneTransition];

                    break;
                }
                case 2:{
                    NSLog(@"Stage 3, partiu!");
                    break;
                }
                default:
                    NSLog(@"Erro no suite");
                    break;
            }
            
            
        }
        if ([lvlselect containsPoint:location])
        {
            
            
            SKView * skView = (SKView *)self.view;
            
                        SKScene *scene = [SelecaoDeFases1 sceneWithSize:skView.bounds.size];
            
                        SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                        [self.view presentScene:scene transition:sceneTransition];
            
            NSLog(@" Seleção de fase");
            
            
        }
        if ([replay containsPoint:location])
        {
            
            SKView * skView = (SKView *)self.view;
            
            switch ([GameData sharedGameData].ultimoStage) {
                case 1:{
                    SKScene *scene = [World1 sceneWithSize:skView.bounds.size];
                    SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                    [self.view presentScene:scene transition:sceneTransition];
                    
                    break;
                }
                case 2:{
                    SKScene *scene = [WorldTeste sceneWithSize:skView.bounds.size];
                    SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                    [self.view presentScene:scene transition:sceneTransition];
                    break;
                }
                default:
                    NSLog(@"Erro no suite");
                    break;
            }
            
            
        }
        
        if([Upgrades containsPoint:location])
        {
            NSLog(@"What are you buying?");
            SKView * skView = (SKView *)self.view;

            SKScene *scene = [UpgradeScene sceneWithSize:skView.bounds.size];
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
        
        
        }
        
    }
}





@end
