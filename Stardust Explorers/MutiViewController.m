//
//  MutiViewController.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 09/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "MutiViewController.h"
#import "MultiBase.h"
#import "GameKitHelper.h"

@interface MutiViewController ()<GameKitHelperDelegate>

@end

@implementation MutiViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerAuthenticated)
                                                 name:@"local_player_authenticated" object:nil];
}

- (void)playerAuthenticated {
    [[GameKitHelper sharedGameKitHelper] findMatchWithMinPlayers:2 maxPlayers:2 viewController:self delegate:self];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// Add new methods to bottom of file
#pragma mark GameKitHelperDelegate

- (void)matchStarted {
    NSLog(@"Match started");
}

- (void)matchEnded {
    NSLog(@"Match ended");
}

- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID {
    NSLog(@"Received data");
}
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // Configure the view.
    
    // Create and configure the scene.
    SKScene * scene = [MultiBase sceneWithSize:self.view.bounds.size];
    scene.backgroundColor = [UIColor darkGrayColor];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    // Present the scene.
    [(SKView *)self.view presentScene:scene];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    NSLog(@"teste");

}


//}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
