//
//  PlayerEngIdol.h
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "Player.h"

@interface PlayerEngIdol : Player
-(void)cooldown1:(NSTimeInterval)tempo;
-(void)cooldown2:(NSTimeInterval)tempo;
@end
