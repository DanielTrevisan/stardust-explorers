//
//  Player.h
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>


@interface Player : SKSpriteNode{
    int pontdevidamax;
    float forcadeataque;
    float coefdefesa;
    int pontdevidaatual;
    int numChaves;
    NSString *spritefiles;
    char playerdirection;
    NSString *estado;
    NSString *estadoHab1;
    NSString *estadoHab2;
    NSString *estadoMago1;
    NSString *estadoMago2;
    NSString *estadoHacker1;
    NSString *estadoHacker2;
    bool estadoPala1;
    bool estadoPala2;
    bool isSelected;
    
}
-(float)ataqueBasico;
-(float)tomarDano;
//-(void)habilidade1;
//-(void)habilidade2;
-(NSString *)getSpriteAtlas;
-(char)getDirection;
-(void)setDirection:(char) direction;
-(int)getHitPoints;
-(void)cooldown1:(NSTimeInterval)tempo;
-(void)cooldown2:(NSTimeInterval)tempo;
-(void)cooldownMago1:(NSTimeInterval)tempo;
-(void)cooldownMago2:(NSTimeInterval)tempo;
-(void)cooldownPala1:(NSTimeInterval)tempo;
-(void)cooldownPala2;
-(void)cooldownHack1:(NSTimeInterval)tempo;
-(void)cooldownHacker2:(NSTimeInterval)tempo;

@property int numChaves;
@property NSString *estadoHab1;
@property NSString *estadoHab2;
@property NSString *estadoMago1;
@property NSString *estadoMago2;
@property NSString *estadoHacker1;
@property NSString *estadoHacker2;
@property bool estadoPala1;
@property bool estadoPala2;
@property int pontdevidaatual;
@property int pontdevidamax;
@property NSString * estado;
@property bool isSelected;
@end
