//
//  WorldBase.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 08/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "JSTileMap.h"
#import "Joystick.h"
#import "PlayerCyberPala.h"
#import "PlayerEngIdol.h"
#import "PlayerHacker.h"
#import "PlayerMateMago.h"
#import "InteractableHack.h"
#import "InteractablePedra.h"
#import "InimigoBase.h"
#import "Projetil.h"
#import "InteractablePortaTrancada.h"
#import "InteractableChave.h"
#import "ObjectPeca.h"
#import "MultiBase.h"



@interface WorldBase : MultiBase<SKPhysicsContactDelegate>{
    IBOutlet UIImageView *stickViewBase;
    IBOutlet UIImageView *stickView;
    Joystick *joystick;
    UIImage *imgStickNormal;
    UIImage *imgStickHold;
    CADisplayLink *velocityTick;
    SKSpriteNode *butMain;
    SKSpriteNode *butA;
    SKSpriteNode *butB;
    SKSpriteNode *vidaP;
    SKSpriteNode *vidaI;
    SKSpriteNode *vidaM;
    SKSpriteNode *vidaH;
    InimigoBase *inimigo;
    NSMutableArray *inimigos;
    NSMutableArray *hacks;
    NSMutableArray *destwalls;
    NSString *abilitySet;
    CGPoint mCenter;
    Player *activePlayer;
    PlayerMateMago *player1;
    PlayerEngIdol *player2;
    PlayerHacker *player3;
    PlayerCyberPala *player4;
    InteractableHack *hack1;
    InteractablePedra *pedra1;
    SKSpriteNode *hp;
    BOOL lock;
    NSMutableArray *portas;
    NSMutableArray *pecas;
    NSMutableArray *chaves;
    NSMutableArray *drones;
    Projetil *paredePala;
    int contador_pecas;
    InteractablePortaTrancada *porta1;
    InteractableChave *chave1;

  //  Projetil *projAuxiliar;
    SKAction *hacksound1;
    SKAction *idolsound1;
    SKAction *palasound1;
    SKAction *magosound1;
}


-(void)addpecas;
-(void)addchave:(NSUInteger)numChaves;
-(void)addcoracoes:(InimigoBase*)Inimigo; //colisão invertida
-(void)randomCoracoes:(InimigoBase*)InimigoBase;
-(void)addPortas:(NSUInteger)numportas;

-(void)addPlayers;
-(void) addObstaculos:(int) numhax andWalls:(int) numwalls;
-(void) addEnemies:(int)num andDrones:(int) numDrones;
-(void) addBotoes;
-(void) addCharaChange;
-(void)checkForAndResolveCollisionsforLayer:(TMXLayer *)layer;
-(void)setViewpointCenter:(CGPoint)position;
-(void)teleporters;
-(void)terminarfase;
-(void)adicionaHP;
-(void)attackEnemies;
-(void)checkForAndResolveCollisionsforLayer:(TMXLayer *)layer andProjectile:(Projetil *)proj;
-(void)trocaClassewithid:(int )numclasse;
-(void)saveData;


@property Projetil* paredePala;
@property Boolean finish;
@property (nonatomic, strong) JSTileMap *map;
@property (strong) TMXLayer *meta;
@property (nonatomic, strong) TMXLayer *walls;
@property (nonatomic, assign) CGPoint desiredPosition;
@property (nonatomic, retain) Player *activePlayer;
@property PlayerCyberPala *player4;
@end
