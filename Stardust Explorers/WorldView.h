//
//  WorldView.h
//  tiletest
//
//  Created by Daniel Trevisan on 21/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface WorldView : SKScene{
    SKSpriteNode *World1pla;
    SKSpriteNode *World2;
    SKSpriteNode *World3;
    SKSpriteNode *World4;
    SKSpriteNode *World5;
    SKSpriteNode *World6;
    SKSpriteNode *World7;
    SKSpriteNode *World8;
    SKLabelNode *Back;
}

@end
