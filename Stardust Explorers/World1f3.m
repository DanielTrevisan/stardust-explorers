//
//  World1f3.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 08/12/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "World1f3.h"
#import "JSTileMap.h"
#import "Joystick.h"
#import "PlayerCyberPala.h"
#import "PlayerEngIdol.h"
#import "PlayerHacker.h"
#import "PlayerMateMago.h"
#import "InteractableHack.h"
#import "WorldBase.h"

@implementation World1f3

-(id)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]) {
        [self.map removeFromParent];
        [self.map removeAllChildren];
        self.physicsWorld.contactDelegate = self;
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.backgroundColor = [SKColor colorWithRed:0 green:0 blue:0 alpha:1.0];
        self.scaleMode = SKSceneScaleModeAspectFill;
        self.map = [JSTileMap mapNamed:@"Fase3Grande.tmx"];
        walls = [self.map layerNamed:@"Meta"];
        walls.layerInfo.visible = false;
        walls.name = @"paredao";
        [self addChild:self.map];
        [self addPlayers];
        [self addEnemies:7 andDrones:0];
        [self addObstaculos:2 andWalls:2];
        [self adicionaHP];
        [self addpecas];
        [self addPortas:1];
        [self addchave:1];
        self.scene.name = @"1";
        NSLog(@"Stage Start!");
        //Joystick
        SKSpriteNode *jsThumb = [SKSpriteNode spriteNodeWithImageNamed:@"joystick"];
        SKSpriteNode *jsBackdrop = [SKSpriteNode spriteNodeWithImageNamed:@"dpad"];
        joystick = [Joystick joystickWithThumb:jsThumb andBackdrop:jsBackdrop];
        joystick.position = CGPointMake(100, 100);
        [self addChild:joystick];
        
        velocityTick = [CADisplayLink displayLinkWithTarget:self selector:@selector(joystickMovement)];
        [velocityTick addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        //Botoes
        [self addBotoes];
        [self addCharaChange];
        [NSTimer scheduledTimerWithTimeInterval:1.0
                                         target:self
                                       selector:@selector(attackEnemies)
                                       userInfo:nil
                                        repeats:YES];
        
        
    }
    return self;
}

-(void)joystickMovement
{
    if ((joystick.velocity.x != 0 || joystick.velocity.y != 0)&& lock== false)
    {
        super.desiredPosition = CGPointMake(activePlayer.position.x + .05 *joystick.velocity.x, activePlayer.position.y + .05 * joystick.velocity.y);
    }
    if (joystick.velocity.x > 0 && ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'r'];
    }
    else if (joystick.velocity.x < 0 && ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'l'];
    }
    if (joystick.velocity.y > 0 && abs(joystick.velocity.y)>abs(joystick.velocity.x) && ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'u'];
    }
    else if (joystick.velocity.y < 0 && abs(joystick.velocity.y)>abs(joystick.velocity.x)&& ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'d'];
    }
}
-(void)timerColisaoProj:(NSTimer *)timer{
    Projetil *proj = [timer userInfo];
    
    [self checkForAndResolveCollisionsforLayer:walls andProjectile:proj];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    [self setViewpointCenter:super.desiredPosition];
    [self checkForAndResolveCollisionsforLayer:walls];
    [self teleporters];
    if(super.finish == NO){
        [self terminarfase];
    }
}


@synthesize walls,meta;

@end
