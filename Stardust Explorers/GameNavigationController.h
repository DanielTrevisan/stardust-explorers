//
//  GameNavigationController.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 06/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameNavigationController : UINavigationController 
- (void)showAuthenticationViewController;
@end
