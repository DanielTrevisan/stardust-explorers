//
//  MenuView.m
//  tiletest
//
//  Created by Daniel Trevisan on 21/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "MenuView.h"
#import "WorldView.h"
#import "GameKitHelper.h"
#import "MultiBase.h"
#import "MutiViewController.h"
#import "GameNavigationController.h"
@implementation MenuView

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"ts_bg.png"];
        
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2);
        bgImage.size = self.size;
        [self addChild:bgImage];
        
        SKSpriteNode *bgMenu = [SKSpriteNode spriteNodeWithImageNamed:@"ts_menu_bg.png"];
        bgMenu.position = CGPointMake(self.size.width/2, (self.size.height/2)-120);
        bgMenu.size = CGSizeMake(500, 250);
        [self addChild:bgMenu];
        
        btnStart = [SKSpriteNode spriteNodeWithImageNamed:@"ts_oneplayer"];
        btnStart.position = CGPointMake(self.size.width/2, (self.size.height/2)-50);
        btnStart.size = CGSizeMake(250, 40);
        [self addChild:btnStart];
        btnMultiStart = [SKSpriteNode spriteNodeWithImageNamed:@"ts_multiplayer"];
        btnMultiStart.size = CGSizeMake(350, 40);

        btnMultiStart.position = CGPointMake(self.size.width/2, self.size.height/3);
        
        [self addChild:btnMultiStart];
        SKSpriteNode *title = [SKSpriteNode spriteNodeWithImageNamed:@"ts_logo"];
        title.position = CGPointMake(self.size.width/2, (self.size.height*4/6)+80);
        [self addChild:title];
        
        btnOptions = [SKSpriteNode spriteNodeWithImageNamed:@"ts_options"];
        btnOptions.size = CGSizeMake(140, 50);

        btnOptions.position = CGPointMake(self.size.width/2, self.size.height/4);
        
        [self addChild:btnOptions];
        
        
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if ([btnStart containsPoint:location])
        {
            SKView * skView = (SKView *)self.view;
            
            SKScene *scene = [WorldView sceneWithSize:skView.bounds.size];
            
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
            GameKitHelper *gamekitHelper = [GameKitHelper sharedGameKitHelper];
            gamekitHelper.enableGameCenter = NO;
            
        }
        if ([btnMultiStart containsPoint:location])
        {
            GameKitHelper *gameKitHelper =[GameKitHelper sharedGameKitHelper];
            
            if (gameKitHelper.enableGameCenter==YES){
            
            SKView * skView = (SKView *)self.view;
            
            SKScene *scene = [MultiBase sceneWithSize:skView.bounds.size];
            
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
            }
            else{
                NSString* title = @"AVISO";
                NSString* message = @"Conecte-se ao Game Center para multiplayer!";
                [GKNotificationBanner showBannerWithTitle: title message: message
                                        completionHandler:^{
                                            GameNavigationController *vc = (GameNavigationController *)self.view.window.rootViewController;
                                            
                                            [vc showAuthenticationViewController];
                                        }];
            }
            
            
        }
        if ([btnOptions containsPoint:location])
        {
            NSLog(@"Opcoes! Implementar");
            
            
        }
        
    }
}

@end
