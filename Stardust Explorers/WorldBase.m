//
//  WorldBase.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 08/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "WorldBase.h"
#import "SKTUtils.h"
#import "FimDeFase.h"
#import "WorldTeste.h"
#import "SelecaoDeFases1.h"
#import "GameData.h"

static const uint32_t enemyCategory = 0x1 << 1;
static const uint32_t projectileCategory = 0x1 << 0;
static const uint32_t haxAbilityCategory = 0x1 << 2;
static const uint32_t haxCategory = 0x1 << 3;
static const uint32_t playerCategory = 0x1 << 4;
static const uint32_t enemyProjectileCategory = 0x1 <<5;
static const uint32_t coracaoCategory = 0x1 << 6;
static const uint32_t pecaCategory = 0x1 << 7;
static const uint32_t chaveCategory = 0x1 << 8;
static const uint32_t PortaCategory = 0x1 << 9;


@implementation WorldBase
    SKLabelNode *progress;
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    

-(id)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]) {
        _finish = NO;
        self.map = nil;
        hacksound1 = [SKAction playSoundFileNamed:@"hackerBasico.wav" waitForCompletion:YES];
        palasound1 = [SKAction playSoundFileNamed:@"palaBasico.wav" waitForCompletion:YES];
        idolsound1 = [SKAction playSoundFileNamed:@"idolBasico.mp3" waitForCompletion:YES];
        magosound1 = [SKAction playSoundFileNamed:@"magoBasico.wav" waitForCompletion:YES];

    }
    return self;
}
- (void)spriteProjetil:(SKSpriteNode *)proj didCollideWithPlayer:(Player *)Player {
    [proj removeFromParent];
    if (![Player.estado  isEqualToString: @"Invis"]) {
        Player.pontdevidaatual--;
        [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [self adicionaHP];
    }
    if(activePlayer.pontdevidaatual <= 0){
        if(activePlayer == player1){
            if(player2.pontdevidaatual >0){
                [self trocaClassewithid:2];
                
            }
            else if (player3.pontdevidaatual>0){
                [self trocaClassewithid:3];

            }
            else if(player4.pontdevidaatual>0){
                [self trocaClassewithid:4];

            }
            else NSLog(@"GAME OVER");
        }
        if(activePlayer == player2){
            if(player3.pontdevidaatual >0){
                [self trocaClassewithid:3];
                
            }
            else if (player4.pontdevidaatual>0){
                [self trocaClassewithid:4];
            }
            else if(player1.pontdevidaatual>0){
                [self trocaClassewithid:1];

            }
            else NSLog(@"GAME OVER");
        }
        if(activePlayer == player3){
            if(player4.pontdevidaatual >0){
                [self trocaClassewithid:4];
                
            }
            else if (player1.pontdevidaatual>0){
                [self trocaClassewithid:1];

            }
            else if(player2.pontdevidaatual>0){
                [self trocaClassewithid:2];

            }
            else NSLog(@"GAME OVER");
        }
        if(activePlayer == player4){
            if(player1.pontdevidaatual >0){
                [self trocaClassewithid:1];
            }
            else if (player2.pontdevidaatual>0){
                [self trocaClassewithid:2];
            }
            else if(player3.pontdevidaatual>0){
                [self trocaClassewithid:3];

            }
            else NSLog(@"GAME OVER");
        }
    }
    
}
-(void)terminarfase{
    TMXObjectGroup *objectgroup= [self.map groupNamed:@"obj1"];
    NSDictionary *despawn = [objectgroup objectNamed:@"despawn"];
    int offx = [despawn[@"x"]integerValue];
    int offy = [despawn[@"y"]integerValue];
    CGPoint exit= CGPointMake(offx, offy);
    CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                   activePlayer.position.y-activePlayer.size.height/2,
                                   activePlayer.frame.size.width,
                                   activePlayer.frame.size.height);
    if (CGRectContainsPoint(playerRect, exit)){
        _finish = YES;
        [self criatimer];
    }
    
}

-(void)adicionaHP{
    //NSString *hpstring = [NSString stringWithFormat:@"Pontos de vida: %d",[activePlayer getHitPoints]];
    for(int i =0;i< [activePlayer getHitPoints];i++){
        hp=[SKSpriteNode spriteNodeWithImageNamed:@"heart"];
        hp.name= @"pontdevida";
        hp.position = CGPointMake(200+hp.size.width*i, 700);
        [self addChild:hp];
    }
}




-(void)criatimer{
    
   // [activePlayer removeFromParent];
    lock = true;
    progress=[SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
    progress.color=[UIColor redColor];
    progress.position = CGPointMake(self.size.width/2, 700);
    [progress setText:@"Fase concluída em: 0:05"];
    [self addChild:progress];
    currMinute=0;
    currSeconds=5;
    [self start];
    
}

-(void)start
{
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}
-(void)timerFired
{
    
    if(currSeconds>0)
    {
        currSeconds-=1;
        
        [progress setText:[NSString stringWithFormat:@"%@%d%@%02d",@"Fase concluída em: ",currMinute,@":",currSeconds]];
    }
    else{
        [self saveData];
        [timer invalidate];
        [self curaAll];
        [self.map removeAllChildren];
        [self.map removeAllActions];
        [self.map removeFromParent];
        [self removeAllChildren];
        [self removeAllActions];
       // [activePlayer release];
        player1.isSelected = NO;
        player2.isSelected = NO;
        player3.isSelected = NO;
        player4.isSelected = NO;
        SKView * skView = (SKView *)self.view;
        [GameData sharedGameData].ultimoStage = self.scene.name.intValue;
        [self removeFromParent];
        SKScene *scene = [FimDeFase sceneWithSize:skView.bounds.size];
        if ([self.scene.name isEqualToString:@"6"]) {
            SKScene *scene = [SelecaoDeFases1 sceneWithSize:skView.bounds.size];
            
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
        }else{
            NSNumber *pecasCol = [NSNumber numberWithInt:contador_pecas];
            scene.userData = [NSMutableDictionary dictionary];
            [scene.userData setObject:pecasCol forKey:@"numPecas"];
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
        }
    }
}

-(void) addObstaculos:(int) numhax andWalls:(int) numwalls{
    TMXObjectGroup *objGroup= [self.map groupNamed:@"obj1"];
    hacks = [NSMutableArray arrayWithCapacity:numhax];

    for (int i=0; i<numhax; i++) {
        NSString *innumber = [NSString stringWithFormat:@"hax%d",i+1];
        NSDictionary *spawnHax = [objGroup objectNamed:innumber];
        hack1 = [InteractableHack new];
        int x= [spawnHax[@"x"] integerValue];
        int y= [spawnHax[@"y"] integerValue];
       // NSLog(@"x: %d y: %d",x,y);
        hack1 = [InteractableHack spriteNodeWithImageNamed:@"hax"];
        [hack1 begin];
        hack1.position = CGPointMake(x, y);
        hack1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:hack1.size];
        hack1.physicsBody.dynamic = NO;
        hack1.physicsBody.categoryBitMask = haxCategory;
        hack1.physicsBody.contactTestBitMask = haxAbilityCategory;
       [NSTimer scheduledTimerWithTimeInterval:0.01 target:self
                                       selector:@selector(timerColisaoHack:)
                                       userInfo:hack1
                                        repeats:YES];
        [self.map addChild:hack1];
        [hacks addObject:hack1];

    }
    destwalls = [NSMutableArray arrayWithCapacity:numwalls];
    for (int i=0; i<numwalls; i++) {
        NSString *innumber = [NSString stringWithFormat:@"wall%d",i+1];
        NSDictionary *spawnWall = [objGroup objectNamed:innumber];
        pedra1 = [InteractablePedra new];
        int x= [spawnWall[@"x"] integerValue];
        int y= [spawnWall[@"y"] integerValue];
        // NSLog(@"x: %d y: %d",x,y);
        pedra1 = [InteractablePedra spriteNodeWithImageNamed:@"rock"];
        pedra1.position = CGPointMake(x, y);
        pedra1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pedra1.size];
        pedra1.physicsBody.dynamic = NO;
        pedra1.physicsBody.categoryBitMask = haxCategory;

        [NSTimer scheduledTimerWithTimeInterval:0.01 target:self
                                       selector:@selector(timerColisaoWall:)
                                       userInfo:pedra1
                                        repeats:YES];
        [self.map addChild:pedra1];
        [destwalls addObject:pedra1];

        
    }

   }
-(void)timerColisaoHack:(NSTimer *)timer{
    InteractableHack *hack = [timer userInfo];
    [self checkForAndResolveCollisionsforHack:hack];
}
-(void)timerColisaoWall:(NSTimer *)timer{
    InteractablePedra *pedra = [timer userInfo];
    if (!pedra.destruido) {
        [self checkForAndResolveCollisionsforPedra:pedra];

    }
}

-(void)addCharaChange{
    //Chara Quadros
    vidaH = [SKSpriteNode spriteNodeWithImageNamed:@"charah.png"];
    vidaI = [SKSpriteNode spriteNodeWithImageNamed:@"charai.png"];
    vidaM = [SKSpriteNode spriteNodeWithImageNamed:@"charaM.png"];
    vidaP = [SKSpriteNode spriteNodeWithImageNamed:@"charap.png"];
    vidaH.position = CGPointMake(self.size.width*15/16, self.size.height*9/10);
    vidaI.position = CGPointMake(self.size.width*15/16, self.size.height*9/10 -1*vidaH.size.height);
    vidaM.position = CGPointMake(self.size.width*15/16, self.size.height*9/10 -2*vidaH.size.height);
    vidaP.position = CGPointMake(self.size.width*15/16, self.size.height*9/10 -3*vidaH.size.height);
    [self addChild:vidaH];
    [self addChild:vidaI];
    [self addChild:vidaM];
    [self addChild:vidaP];
}

-(void)addBotoes{
    butA = [SKSpriteNode spriteNodeWithImageNamed:@"buth1.png"];
    butB = [SKSpriteNode spriteNodeWithImageNamed:@"buth2.png"];
    butMain= [SKSpriteNode spriteNodeWithImageNamed:@"but1.png"];
    butA.position = CGPointMake(self.size.width*5/6, self.size.height*2/8);
    butB.position = CGPointMake(self.size.width*15/16, self.size.height*2/8);
    butMain.position = CGPointMake(self.size.width*7/8, self.size.height*1/8);
    butMain.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:butMain.size.width/2];
    butMain.physicsBody.dynamic = NO;
    butA.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:butA.size.width/2];
    butA.physicsBody.dynamic = NO;
    butB.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:butA.size.width/2];
    butB.physicsBody.dynamic = NO;
    [self addChild:butA];
    [self addChild:butB];
    [self addChild:butMain];
}

-(void)addPlayers{
    
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    self.activePlayer = [[Player alloc]init];
    player1 = [PlayerMateMago spriteNodeWithImageNamed:@"metamage_v3_6060"];
    player2 = [PlayerEngIdol spriteNodeWithImageNamed:@"muki_6060"];
    player3 = [PlayerHacker spriteNodeWithImageNamed:@"hacker_v2_6060"];
    player4 = [PlayerCyberPala spriteNodeWithImageNamed:@"paladin_v3"];
//    NSData *play1 = [NSKeyedArchiver archivedDataWithRootObject:player1];
//    NSData *play2 = [NSKeyedArchiver archivedDataWithRootObject:player2];
//    NSData *play3 = [NSKeyedArchiver archivedDataWithRootObject:player3];
//    NSData *play4 = [NSKeyedArchiver archivedDataWithRootObject:player4];
//    [prefs setObject:play1 forKey:@"pleya1"];
//    [prefs setObject:play2 forKey:@"pleya2"];
//    [prefs setObject:play3 forKey:@"pleya3"];
//    [prefs setObject:play4 forKey:@"pleya4"];
//    [prefs synchronize];
    if([super ultimaclasse] ==kClassHacker && [GameKitHelper sharedGameKitHelper].enableGameCenter){
        activePlayer = player3;
        player3.isSelected = YES;

    }
    else if([super ultimaclasse] ==kClassIdol && [GameKitHelper sharedGameKitHelper].enableGameCenter){
        activePlayer = player2;
        player2.isSelected = YES;

        
    }
    else if([super ultimaclasse] ==kClassPaladin && [GameKitHelper sharedGameKitHelper].enableGameCenter){
        activePlayer = player4;
        player4.isSelected = YES;

        
    }
    else{
        activePlayer = player1;
        player1.isSelected = YES;

    }
    TMXObjectGroup *objectgroup= [self.map groupNamed:@"obj1"];
    
    NSDictionary *spawnPoint = [objectgroup objectNamed:@"spawnpoint"];
    
    int x = [spawnPoint[@"x"] integerValue];
    int y = [spawnPoint[@"y"] integerValue];
    activePlayer.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:activePlayer.size];
    activePlayer.physicsBody.dynamic = NO;
    activePlayer.position = CGPointMake(x, y);
    player1.position = activePlayer.position;
    player2.position = activePlayer.position;
    player3.position = activePlayer.position;
    player4.position = activePlayer.position;
    activePlayer.physicsBody.categoryBitMask = playerCategory;
    _desiredPosition = activePlayer.position;
    [activePlayer setDirection:'d'];

    [self.map addChild:activePlayer];
    
}

-(void) addEnemies:(int) num andDrones:(int)numDrones{
    inimigos = [NSMutableArray arrayWithCapacity:num];
    for(int i =1; i<=num;i++){
        inimigo = [InimigoBase spriteNodeWithImageNamed:@"boringdude"];
        TMXObjectGroup *enemygroup= [self.map groupNamed:@"inimigos"];
        NSString *innumber = [NSString stringWithFormat:@"in%d",i];
        NSDictionary *spawnEnemy = [enemygroup objectNamed:innumber];
        int x= [spawnEnemy[@"x"] integerValue];
        int y= [spawnEnemy[@"y"] integerValue];
        NSLog(@"x: %d, y:%d",x,y);
        inimigo.position = CGPointMake(x, y);
        inimigo.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:inimigo.size];
        inimigo.physicsBody.categoryBitMask = enemyCategory;
        inimigo.physicsBody.contactTestBitMask = projectileCategory;
        inimigo.physicsBody.collisionBitMask = 1;
        inimigo.physicsBody.dynamic = NO;
        inimigo.pontdevidaatual = 5;
        [self.map addChild:inimigo];
        [inimigos addObject:inimigo];
    }
    drones = [NSMutableArray arrayWithCapacity:numDrones];
    for (int i=0; i<numDrones; i++) {
        inimigo = [InimigoBase spriteNodeWithImageNamed:@"drone"];
        TMXObjectGroup *enemygroup= [self.map groupNamed:@"inimigos"];
        NSString *innumber = [NSString stringWithFormat:@"drone1"];
        NSDictionary *spawnEnemy = [enemygroup objectNamed:innumber];
        int x= [spawnEnemy[@"x"] integerValue];
        int y= [spawnEnemy[@"y"] integerValue];
        inimigo.position = CGPointMake(x, y);
        inimigo.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:inimigo.size];
        inimigo.physicsBody.dynamic= YES;
        inimigo.physicsBody.categoryBitMask = enemyCategory;
        inimigo.physicsBody.contactTestBitMask = projectileCategory;
        inimigo.physicsBody.collisionBitMask = 1;
        inimigo.physicsBody.dynamic = NO;
        inimigo.pontdevidaatual = 5;
        [self.map addChild:inimigo];
        [drones addObject:inimigo];
    }
    
}

-(void)teleporters{
    TMXObjectGroup *tele1= [self.map groupNamed:@"obj1"];
    NSDictionary *telePoint1 = [tele1 objectNamed:@"teleport1"];
    NSDictionary *telePoint2 = [tele1 objectNamed:@"teleport 2"];
    int xtele = [telePoint1[@"x"] integerValue];
    int ytele = [telePoint1[@"y"] integerValue];
    int xtele2 = [telePoint2[@"x"] integerValue];
    int ytele2 = [telePoint2[@"y"] integerValue];
    CGPoint tel1= CGPointMake(xtele, ytele);
    CGPoint tel2= CGPointMake(xtele2, ytele2);
    CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                   activePlayer.position.y-activePlayer.size.height/2,
                                   activePlayer.frame.size.width,
                                   activePlayer.frame.size.height);
    if (CGRectContainsPoint(playerRect, tel1)){
        activePlayer.position=tel2;
        
    }
}

-(void)joystickMovement
{
    if (joystick.velocity.x != 0 || joystick.velocity.y != 0)
    {
        _desiredPosition = CGPointMake(activePlayer.position.x + .05 *joystick.velocity.x, activePlayer.position.y + .05 * joystick.velocity.y);
        {
        }
    }
    if (joystick.velocity.x > 0)
    {
        [activePlayer setDirection:'r'];
    }
    else if (joystick.velocity.x < 0)
    {
        [activePlayer setDirection:'l'];
    }
    if (joystick.velocity.y > 0 && abs(joystick.velocity.y)>abs(joystick.velocity.x))
    {
        [activePlayer setDirection:'u'];
    }
    else if (joystick.velocity.y < 0 && abs(joystick.velocity.y)>abs(joystick.velocity.x))
    {
        [activePlayer setDirection:'d'];
    }
}
- (void)setViewpointCenter:(CGPoint)position {
    NSInteger x = MAX(position.x, self.size.width / 2);
    NSInteger y = MAX(position.y, self.size.height / 2);
    x = MIN(x, (self.map.mapSize.width * self.map.tileSize.width) - self.size.width / 2);
    y = MIN(y, (self.map.mapSize.height * self.map.tileSize.height) - self.size.height / 2);
    CGPoint actualPosition = CGPointMake(x, y);
    CGPoint centerOfView = CGPointMake(self.size.width/2, self.size.height/2);
    CGPoint viewPoint = CGPointSubtract(centerOfView, actualPosition);
    self.map.position = viewPoint;
}
- (CGPoint)tileCoordForPosition:(CGPoint)position {
    int x = position.x / _map.tileSize.width;
    int y = ((_map.mapSize.height * _map.tileSize.height) - position.y) / _map.tileSize.height;
    return CGPointMake(x, y);
}
-(CGRect)tileRectFromTileCoords:(CGPoint)tileCoords {
    float levelHeightInPixels = self.map.mapSize.height * self.map.tileSize.height;
    CGPoint origin = CGPointMake(tileCoords.x * self.map.tileSize.width, levelHeightInPixels - ((tileCoords.y + 1) * self.map.tileSize.height));
    return CGRectMake(origin.x, origin.y, self.map.tileSize.width, self.map.tileSize.height);
}

- (NSInteger)tileGIDAtTileCoord:(CGPoint)coord forLayer:(TMXLayer *)layer {
    TMXLayerInfo *layerInfo = layer.layerInfo;
    return [layerInfo tileGidAtCoord:coord];
}


-(void)didBeginContact:(SKPhysicsContact *) contact{
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask & projectileCategory) != 0 &&
        (secondBody.categoryBitMask & enemyCategory) != 0)
    {
        [self spriteProjetil:(Projetil *)firstBody.node didCollideWithEnemy: (InimigoBase *)secondBody.node];
    }
    if ((firstBody.categoryBitMask & haxAbilityCategory) != 0 &&
        (secondBody.categoryBitMask & haxCategory) != 0)
    {
        NSLog(@"AAA");
        
        [(SKSpriteNode *)firstBody.node removeFromParent];
        [(SKSpriteNode *)secondBody.node removeFromParent];
    }
    ///Dano no player
    if ((firstBody.categoryBitMask & playerCategory) != 0 &&
        (secondBody.categoryBitMask & enemyProjectileCategory) != 0)
    {
        NSLog(@"AAA");
        [self spriteProjetil:(SKSpriteNode *)secondBody.node didCollideWithPlayer: (Player *)firstBody.node];
    }
    //Item de heal
    if ((firstBody.categoryBitMask & playerCategory) != 0 &&
        (secondBody.categoryBitMask & coracaoCategory) != 0)
    {
        NSLog(@"HEAL");
        [self spriteCoracao:(SKSpriteNode *)secondBody.node didCollideWithPlayer: (Player *)firstBody.node];
    }
    //Peças
    if ((firstBody.categoryBitMask & playerCategory) != 0 &&
        (secondBody.categoryBitMask & pecaCategory) != 0)
    {
        NSLog(@"Peça!");
        [self spritePeca:(SKSpriteNode *)secondBody.node didCollideWithPlayer: (Player *)firstBody.node];
    }
    
    if ((firstBody.categoryBitMask & playerCategory) != 0 &&
        (secondBody.categoryBitMask & chaveCategory) != 0)
    {
        NSLog(@"Chave!");
        [self spriteChave:(SKSpriteNode *)secondBody.node didCollideWithPlayer: (Player *)firstBody.node];
    }
    
    if ((firstBody.categoryBitMask & playerCategory) != 0 &&
        (secondBody.categoryBitMask & PortaCategory) != 0)
    {
        NSLog(@"Porta!");
        [self spritePorta:(SKSpriteNode *)secondBody.node didCollideWithPlayer: (Player *)firstBody.node];
    }
    if ((firstBody.categoryBitMask & enemyProjectileCategory) != 0 &&
        (secondBody.categoryBitMask & PortaCategory) != 0)
    {
        NSLog(@"Porta Projetil!");
        [firstBody.node removeFromParent];
    }
    
}


-(void)attackEnemies{
    for (int i=0; i<[drones count]; i++) {
        
    if (![activePlayer.estado  isEqualToString: @"Invis"] ) {
        SKSpriteNode *projetil = [Projetil spriteNodeWithImageNamed:@"tiro1"];
        InimigoBase *inimigTeste = [drones objectAtIndex:0];
        if(![inimigTeste.estado  isEqual: @"morto"] ){
            projetil.position = inimigTeste.position;
            projetil.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:projetil.size.width/8];
            projetil.physicsBody.categoryBitMask = enemyProjectileCategory;
            projetil.physicsBody.contactTestBitMask = playerCategory | PortaCategory;
            projetil.physicsBody.collisionBitMask = 0;
            projetil.physicsBody.dynamic= YES;
            projetil.physicsBody.usesPreciseCollisionDetection = YES;
            [self.map addChild:projetil];
            SKAction *move = [SKAction moveTo:CGPointMake(activePlayer.position.x , activePlayer.position.y) duration:3];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [projetil runAction:[SKAction sequence:@[[SKAction runBlock:^{
                [NSTimer scheduledTimerWithTimeInterval:0.05
                                                 target:self
                                               selector:@selector(timerColisaoProj:)
                                               userInfo:projetil
                                                repeats:YES];
                [projetil runAction:move completion:^{
                    [projetil runAction:actionMoveDone];
                }];
            }]]]];
        }
        }
    }
}
-(void)timerColisaoPorta:(NSTimer *)timer{
    InteractablePortaTrancada *porta = [timer userInfo];
    [self checkForAndResolveCollisionsforPorta:porta];
}


-(void)addpecas{
    contador_pecas = 0;
    
    TMXObjectGroup *pecasobj = [self.map groupNamed:@"obj1"];
    NSDictionary *peca1 = [pecasobj objectNamed:@"peca1"];
    NSDictionary *peca2 = [pecasobj objectNamed:@"peca2"];
    NSDictionary *peca3 = [pecasobj objectNamed:@"peca3"];
    NSDictionary *peca4 = [pecasobj objectNamed:@"peca4"];
    int xpeca1 = [peca1[@"x"]integerValue];
    int ypeca1 = [peca1[@"y"]integerValue];
    int xpeca2 = [peca2[@"x"]integerValue];
    int ypeca2 = [peca2[@"y"]integerValue];
    int xpeca3 = [peca3[@"x"]integerValue];
    int ypeca3 = [peca3[@"y"]integerValue];
    int xpeca4 = [peca4[@"x"]integerValue];
    int ypeca4 = [peca4[@"y"]integerValue];
    
    SKSpriteNode *Pecas1 = [ObjectPeca spriteNodeWithImageNamed:@"peca.png"];
    Pecas1.position = CGPointMake(xpeca1, ypeca1);
    Pecas1.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:Pecas1.size.width/4];
    Pecas1.physicsBody.categoryBitMask = pecaCategory;
    Pecas1.physicsBody.contactTestBitMask = playerCategory;
    Pecas1.physicsBody.collisionBitMask = 0;
    Pecas1.physicsBody.dynamic= YES;
    Pecas1.physicsBody.usesPreciseCollisionDetection = YES;
    
    
    SKSpriteNode *Pecas2= [ObjectPeca spriteNodeWithImageNamed:@"peca.png"];
    Pecas2.position = CGPointMake(xpeca2, ypeca2);
    Pecas2.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:Pecas2.size.width/4];
    Pecas2.physicsBody.categoryBitMask = pecaCategory;
    Pecas2.physicsBody.contactTestBitMask = playerCategory;
    Pecas2.physicsBody.collisionBitMask = 0;
    Pecas2.physicsBody.dynamic= YES;
    Pecas2.physicsBody.usesPreciseCollisionDetection = YES;
    
    SKSpriteNode *Pecas3= [ObjectPeca spriteNodeWithImageNamed:@"peca.png"];
    Pecas3.position = CGPointMake(xpeca3, ypeca3);
    Pecas3.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:Pecas3.size.width/4];
    Pecas3.physicsBody.categoryBitMask = pecaCategory;
    Pecas3.physicsBody.contactTestBitMask = playerCategory;
    Pecas3.physicsBody.collisionBitMask = 0;
    Pecas3.physicsBody.dynamic= YES;
    Pecas3.physicsBody.usesPreciseCollisionDetection = YES;
    
    SKSpriteNode *Pecas4= [ObjectPeca spriteNodeWithImageNamed:@"peca.png"];
    Pecas4.position = CGPointMake(xpeca4, ypeca4);
    Pecas4.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:Pecas4.size.width/4];
    Pecas4.physicsBody.categoryBitMask = pecaCategory;
    Pecas4.physicsBody.contactTestBitMask = playerCategory;
    Pecas4.physicsBody.collisionBitMask = 0;
    Pecas4.physicsBody.dynamic= YES;
    Pecas4.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self.map addChild:Pecas1];
    [self.map addChild:Pecas2];
    [self.map addChild:Pecas3];
    [self.map addChild:Pecas4];
};

- (void)spritePeca:(SKSpriteNode *)peca didCollideWithPlayer:(Player *)Player {
    [peca removeFromParent];
    contador_pecas++;
    NSLog(@"Peças coletadas: %d de 4", contador_pecas);
}

-(void)spriteChave:(SKSpriteNode *)chave didCollideWithPlayer:(Player *)Player{
    [chave removeFromParent];
    player1.numChaves++;
    player2.numChaves++;
    player3.numChaves++;
    player4.numChaves++;
    
    NSLog(@"Número de chaves: %d", activePlayer.numChaves);
}

-(void)spritePorta:(SKSpriteNode *)porta didCollideWithPlayer:(Player *)Player{
    if(activePlayer.numChaves > 0){
        [porta removeFromParent];
        player1.numChaves--;
        player2.numChaves--;
        player3.numChaves--;
        player4.numChaves--;
    }
}

-(void)addchave:(NSUInteger)numChaves{
    TMXObjectGroup *objGroup= [self.map groupNamed:@"obj1"];
    chaves = [NSMutableArray arrayWithCapacity:numChaves];
    
    for (int i=0; i<numChaves; i++) {
        NSString *innumber = [NSString stringWithFormat:@"chave%d",i+1];
        NSDictionary *spawnChave = [objGroup objectNamed:innumber];
        chave1 = [InteractableChave new];
        int x= [spawnChave[@"x"] integerValue];
        int y= [spawnChave[@"y"] integerValue];
        // NSLog(@"x: %d y: %d",x,y);
        chave1 = [InteractableChave spriteNodeWithImageNamed:@"chave"];
        //        [chave1 begin];
        chave1.position = CGPointMake(x, y);
        chave1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:chave1.size];
        chave1.physicsBody.dynamic = YES;
        chave1.physicsBody.categoryBitMask = chaveCategory;
        chave1.physicsBody.contactTestBitMask = playerCategory;
        chave1.physicsBody.collisionBitMask = 0;
        [self.map addChild:chave1];
        [chaves addObject:chave1];
    }
};



-(void)addPortas:(NSUInteger)numportas{
    ////////////////////////////////
    TMXObjectGroup *objGroup= [self.map groupNamed:@"obj1"];
    portas = [NSMutableArray arrayWithCapacity:numportas];
    
    for (int i=0; i<numportas; i++) {
        NSString *innumber = [NSString stringWithFormat:@"porta%d",i+1];
        NSDictionary *spawnPorta = [objGroup objectNamed:innumber];
        porta1 = [InteractablePortaTrancada new];
        int x= [spawnPorta[@"x"] integerValue];
        int y= [spawnPorta[@"y"] integerValue];
        // NSLog(@"x: %d y: %d",x,y);
        porta1 = [InteractablePortaTrancada spriteNodeWithImageNamed:@"porta"];
        [porta1 begin];
        porta1.position = CGPointMake(x, y);
        porta1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:porta1.size];
        porta1.physicsBody.dynamic = NO;
        porta1.physicsBody.categoryBitMask = PortaCategory;
        porta1.physicsBody.contactTestBitMask = playerCategory;
        [NSTimer scheduledTimerWithTimeInterval:0.01 target:self
                                       selector:@selector(timerColisaoPorta:)
                                       userInfo:porta1
                                        repeats:YES];
        [self.map addChild:porta1];
        [portas addObject:porta1];
        
    }
}


- (void)checkForAndResolveCollisionsforPorta:(InteractablePortaTrancada *)porta {
    if ((porta.ativado == true)&&(activePlayer.numChaves == 0)) {
        CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                       activePlayer.position.y-activePlayer.size.height/2,
                                       activePlayer.frame.size.width,
                                       activePlayer.frame.size.height);
        CGRect PortaRect = CGRectMake(porta.position.x-porta.size.width/2,
                                      porta.position.y-porta.size.height/2,
                                      porta.frame.size.width,
                                      porta.frame.size.height);
        
        if (CGRectIntersectsRect(playerRect, PortaRect)) {
            // [activePlayer removeAllActions];
            CGRect intersection = CGRectIntersection(playerRect, PortaRect);
            //2
            if (activePlayer.position.y<porta.position.y && intersection.size.width>=intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y - intersection.size.height);
            } else if (activePlayer.position.y>porta.position.y && intersection.size.width>=intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y + intersection.size.height);
            } else if (activePlayer.position.x<porta.position.x && intersection.size.width<intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x - intersection.size.width, _desiredPosition.y);
            } else if (activePlayer.position.x>porta.position.x && intersection.size.width<intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x + intersection.size.width, _desiredPosition.y);
            }
        }
        
        
        
        //5
        activePlayer.position = _desiredPosition;
        
    }
    else if((porta.ativado == true)&&(activePlayer.numChaves > 0)){
        
        CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                       activePlayer.position.y-activePlayer.size.height/2,
                                       activePlayer.frame.size.width,
                                       activePlayer.frame.size.height);
        CGRect PortaRect = CGRectMake(porta.position.x-porta.size.width/2,
                                      porta.position.y-porta.size.height/2,
                                      porta.frame.size.width,
                                      porta.frame.size.height);
        
        if (CGRectIntersectsRect(playerRect, PortaRect)) {
            porta.ativado = false;
            [porta removeFromParent];
            player1.numChaves--;
            player2.numChaves--;
            player3.numChaves--;
            player4.numChaves--;
        }
        
    }
    else if(porta.ativado==false){
        
    }
}




-(void)addcoracoes:(InimigoBase*)Inimigo{
    SKSpriteNode *cora = [Projetil spriteNodeWithImageNamed:@"heart"];
    if([Inimigo.estado  isEqual: @"morto"] ){
        cora.position = Inimigo.position;
        cora.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:cora.size];
        cora.physicsBody.categoryBitMask = coracaoCategory;
        cora.physicsBody.contactTestBitMask = playerCategory;
        cora.physicsBody.collisionBitMask = 6;
        cora.physicsBody.dynamic= YES;
        cora.physicsBody.usesPreciseCollisionDetection = YES;
        [Inimigo removeFromParent];
        SKAction *wait = [SKAction waitForDuration:0.2];
        [self runAction:wait completion:^{
            NSLog(@"teste cria cora");
            [self.map addChild:cora];
            

        }];

    }
    
};

-(void)randomCoracoes:(InimigoBase*)Inimigo{
    int i = arc4random_uniform(5);
    if(player1.getHitPoints + player2.getHitPoints + player3.getHitPoints + player4.getHitPoints <= 8){
        i ++;
    }
    if(i >= 2){
        [self addcoracoes:Inimigo];
    }
    else{
        [Inimigo removeFromParent];

    }

    
}

- (void)spriteCoracao:(SKSpriteNode *)cora didCollideWithPlayer:(Player *)Player {
    [cora removeFromParent];
    Player.pontdevidaatual++;
    [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];
    [self adicionaHP];
    
}

- (void)spriteProjetil:(Projetil *)proj didCollideWithEnemy:(InimigoBase *)inim {
    inim.pontdevidaatual = inim.pontdevidaatual - proj.danoProj;
    NSLog(@"VIDA INIMIGO: %d", inim.pontdevidaatual);
    if (inim.pontdevidaatual<=0){
        //        [inim removeFromParent];
        
        inim.estado = @"morto";
        [self randomCoracoes:inim];
    }
    
}

-(void)timerColisaoProj:(NSTimer *)timer{
    Projetil *proj = [timer userInfo];
    [self checkForAndResolveCollisionsforLayer:walls andProjectile:proj];
}

- (void)checkForAndResolveCollisionsforLayer:(TMXLayer *)layer {
    //1
    NSInteger indices[8] = {1,2,3,4,5,6,7,8};
    for (NSUInteger i = 0; i < 8; i++) {
        NSInteger tileIndex = indices[i];
        //2
        CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                       activePlayer.position.y-activePlayer.size.height/2,
                                       activePlayer.frame.size.width,
                                       activePlayer.frame.size.height);
        //3
        CGPoint playerCoord = [layer coordForPoint:activePlayer.position];
        //4
        NSInteger tileColumn = tileIndex % 3;
        NSInteger tileRow = tileIndex / 3;
        CGPoint tileCoord = CGPointMake(playerCoord.x + (tileColumn - 1), playerCoord.y + (tileRow - 1));
        //5
        NSInteger gid = [self tileGIDAtTileCoord:tileCoord forLayer:layer];
        //6
        if (gid) {
            //7
            CGRect tileRect = [self tileRectFromTileCoords:tileCoord];
            //8
            //NSLog(@"GID %ld, Tile Coord %@, Tile Rect %@, player rect %@", (long)gid, NSStringFromCGPoint(tileCoord), NSStringFromCGRect(tileRect), NSStringFromCGRect(playerRect));
            //collision resolution goes here
            //1
            if (CGRectIntersectsRect(playerRect, tileRect)) {
                
                
                if ([activePlayer.estado isEqualToString:@"Investida"]) {
                    activePlayer.estado = @"pronto";
                    [activePlayer removeAllActions];
                }
                CGRect intersection = CGRectIntersection(playerRect, tileRect);
                
                //2
                if (tileIndex == 7) {
                    //tile is directly below Koala
                    _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y + intersection.size.height);
                } else if (tileIndex == 1) {
                    //tile is directly above Koala
                    _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y - intersection.size.height);
                } else if (tileIndex == 3) {
                    //tile is left of Koala
                    _desiredPosition = CGPointMake(_desiredPosition.x + intersection.size.width, _desiredPosition.y);
                } else if (tileIndex == 5) {
                    //tile is right of Koala
                    _desiredPosition = CGPointMake(_desiredPosition.x - intersection.size.width, _desiredPosition.y);
                    //3
                } else {
//                    if (intersection.size.width > intersection.size.height) {
//                        //tile is diagonal, but resolving collision vertically
//                        //4
//                        NSLog(@"quack :V:");
//                        float intersectionHeight;
//                        if (tileIndex > 4) {
//                            intersectionHeight = intersection.size.height;
//                        } else {
//                            intersectionHeight = -intersection.size.height;
//                        }
//                        _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y + intersection.size.height );
//                    } else {
                        //tile is diagonal, but resolving horizontally
                        
//                        float intersectionWidth;
//                        if (tileIndex == 6 || tileIndex == 0) {
//                            intersectionWidth = intersection.size.width;
//                        } else {
//                            intersectionWidth = -intersection.size.width;
//                        }
//                        _desiredPosition = CGPointMake(_desiredPosition.x  + intersectionWidth, _desiredPosition.y);
                    //}
                }
            }
        }
    }
    //5
    activePlayer.position = _desiredPosition;
}
-(void)checkForAndResolveCollisionsforLayer:(TMXLayer *)layer andProjectile:(Projetil *)proj{
    //1
    NSInteger indices[8] = {1,2,3,4,5,6,7,8};
    for (NSUInteger i = 0; i < 8; i++) {

       NSInteger tileIndex = indices[i];
        
        //2
//        CGPoint projRect = CGRectMake(proj.position.x-proj.size.width/2,
//                                       proj.position.y-proj.size.height/2,
//                                       proj.frame.size.width,
//                                       proj.frame.size.height);
        //3
        CGPoint projCoord = [layer coordForPoint:proj.position];
        //4
        NSInteger tileColumn = tileIndex % 3;
        NSInteger tileRow = tileIndex / 3;
        CGPoint tileCoord = CGPointMake(projCoord.x + (tileColumn - 1), projCoord.y + (tileRow - 1));
        //5
        NSInteger gid = [self tileGIDAtTileCoord:tileCoord forLayer:layer];
//        NSLog(@"GID %ld, Tile Coord %@, proj rect %@", (long)gid, NSStringFromCGPoint(tileCoord), NSStringFromCGRect(projRect));
        if (gid) {
            CGRect tileRect = [self tileRectFromTileCoords:tileCoord];
            if (CGRectContainsPoint (tileRect,proj.position)) {
                [proj removeAllActions];
                [proj removeFromParent];
                       }
    }
    }
    //5
}
- (void)checkForAndResolveCollisionsforHack:(InteractableHack *)interac {
    if (interac.ativado == true) {
        CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                       activePlayer.position.y-activePlayer.size.height/2,
                                       activePlayer.frame.size.width,
                                       activePlayer.frame.size.height);
        CGRect hackRect = CGRectMake(interac.position.x-interac.size.width/2,
                                     interac.position.y-interac.size.height/2,
                                     interac.frame.size.width,
                                     interac.frame.size.height);
        
        if (CGRectIntersectsRect(playerRect, hackRect)) {
           // [activePlayer removeAllActions];
            CGRect intersection = CGRectIntersection(playerRect, hackRect);
            //2
            if (activePlayer.position.y<interac.position.y && intersection.size.width>=intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y - intersection.size.height);
            } else if (activePlayer.position.y>interac.position.y && intersection.size.width>=intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y + intersection.size.height);
            } else if (activePlayer.position.x<interac.position.x && intersection.size.width<intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x - intersection.size.width, _desiredPosition.y);
            } else if (activePlayer.position.x>interac.position.x && intersection.size.width<intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x + intersection.size.width, _desiredPosition.y);
            }
        }
        
        
        //5
        activePlayer.position = _desiredPosition;

    }
       
}

- (void)checkForAndResolveCollisionsforPedra:(InteractablePedra *)interac {
    if (![activePlayer.estado  isEqual: @"Investida"] && !interac.destruido) {
        CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                       activePlayer.position.y-activePlayer.size.height/2,
                                       activePlayer.frame.size.width,
                                       activePlayer.frame.size.height);
        CGRect hackRect = CGRectMake(interac.position.x-interac.size.width/2,
                                     interac.position.y-interac.size.height/2,
                                     interac.frame.size.width,
                                     interac.frame.size.height);
        
        if (CGRectIntersectsRect(playerRect, hackRect)) {
           [activePlayer removeAllActions];
            CGRect intersection = CGRectIntersection(playerRect, hackRect);
            //2
            if (activePlayer.position.y<interac.position.y && intersection.size.width>=intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y - intersection.size.height);
            } else if (activePlayer.position.y>interac.position.y && intersection.size.width>=intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y + intersection.size.height);
            } else if (activePlayer.position.x<interac.position.x && intersection.size.width<intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x - intersection.size.width, _desiredPosition.y);
            } else if (activePlayer.position.x>interac.position.x && intersection.size.width<intersection.size.height) {
                _desiredPosition = CGPointMake(_desiredPosition.x + intersection.size.width, _desiredPosition.y);
            }
        }
        
        
        //5
        activePlayer.position = _desiredPosition;
        
    }
    else{
        CGRect playerRect = CGRectMake(activePlayer.position.x-activePlayer.size.width/2,
                                       activePlayer.position.y-activePlayer.size.height/2,
                                       activePlayer.frame.size.width,
                                       activePlayer.frame.size.height);
        CGRect hackRect = CGRectMake(interac.position.x-interac.size.width/2,
                                     interac.position.y-interac.size.height/2,
                                     interac.frame.size.width,
                                     interac.frame.size.height);
        if (CGRectIntersectsRect(playerRect, hackRect)) {
            [activePlayer removeAllActions];
            activePlayer.estado = @"pronto";
            [interac destruirPedra];
            [interac removeFromParent];
        }
        
    }
        
    //3
    //        } else {
    //            if (intersection.size.width > intersection.size.height) {
    //                //tile is diagonal, but resolving collision vertically
    //                //4
    //                float intersectionHeight;
    //                if (tileIndex > 4) {
    //                    intersectionHeight = intersection.size.height;
    //                } else {
    //                    intersectionHeight = -intersection.size.height;
    //                }
    //                _desiredPosition = CGPointMake(_desiredPosition.x, _desiredPosition.y + intersection.size.height );
    //            } else {
    //                //tile is diagonal, but resolving horizontally
    //                float intersectionWidth;
    //                if (tileIndex == 6 || tileIndex == 0) {
    //                    intersectionWidth = intersection.size.width;
    //                } else {
    //                    intersectionWidth = -intersection.size.width;
    //                }
    //                _desiredPosition = CGPointMake(_desiredPosition.x  + intersectionWidth, _desiredPosition.y);
    //            }
    
}
#pragma -mark  Habilidades
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
#pragma -mark ataques basicos
        if ([butMain containsPoint:location]&& [activePlayer isKindOfClass:[PlayerMateMago class]] )
        {
            [self autoAtaqueMago];
        }
        if([butMain containsPoint:location]&& [activePlayer isKindOfClass:[PlayerEngIdol class]]){

            [self autoAtaqueIdol];
            
        }
        if([butMain containsPoint:location]&& [activePlayer isKindOfClass:[PlayerHacker class]]){
            [self autoAtaqueHacker];
        }
        if([butMain containsPoint:location]&& [activePlayer isKindOfClass:[PlayerCyberPala class]]){
            [self autoAtaquePaladino];
        }
        
#pragma -mark Habilidade 1
        if ([butA containsPoint:location] && [activePlayer isKindOfClass:[PlayerMateMago class]])
        {
            [self hab1Mago];
        }
        if ([butA containsPoint:location] && [activePlayer isKindOfClass:[PlayerEngIdol class]]){
            
            [self hab1Idol];
        }
        if ([butA containsPoint:location] && [activePlayer isKindOfClass:[PlayerHacker class]]){
            [self hab1Hacker];
        }
        
        if ([butA containsPoint:location] && [activePlayer isKindOfClass:[PlayerCyberPala class]]){
            [self hab1Paladino];
        }
#pragma -mark HabilidadesB
        if ([butB containsPoint:location] && [activePlayer isKindOfClass:[PlayerMateMago class]])
        {
            [self hab2Mago];
        }
        //IDOL HAB B
        if ([butB containsPoint:location] && [activePlayer isKindOfClass:[PlayerEngIdol class]]){
            [self hab2Idol];
            
        }
        //Hacker Hab B
        if ([butB containsPoint:location] && [activePlayer isKindOfClass:[PlayerHacker class]]){
            [self hab2Hacker];
        }
        //Cyber Pala Hab B
        if ([butB containsPoint:location] && [activePlayer isKindOfClass:[PlayerCyberPala class]]){
            [self hab2Paladino];
        }
#pragma -mark trocabuttons
        if ([vidaH containsPoint:location]){
            [self trocaClassewithid:3];
        }
        if ([vidaI containsPoint:location]) {
            [self trocaClassewithid:2];
        }
        if ([vidaM containsPoint:location]) {
            [self trocaClassewithid:1];
        }
        if ([vidaP containsPoint:location]) {
            [self trocaClassewithid:4];
        }
        
    }
    
}

-(void)trocaClassewithid:(int )numclasse{
    CGPoint ponto = activePlayer.position;
    [activePlayer removeFromParent];
//    NSLog(@"active player: %f , %f", activePlayer.position.x, activePlayer.position.y);
//    NSLog(@"des pos: %f , %f", _desiredPosition.x, _desiredPosition.y);
    if (numclasse ==1 && player1.isSelected == NO && player1.pontdevidaatual >0) {
        if ([activePlayer isKindOfClass:[PlayerEngIdol class]]) {
            player2.isSelected= NO;
        }
        else if([activePlayer isKindOfClass:[PlayerHacker class]]){
            player3.isSelected=NO;
        }
        else if([activePlayer isKindOfClass:[PlayerCyberPala class]]){
            player4.isSelected=NO;
        }
        activePlayer = player1;
        player1.isSelected = YES;

    }
    else if (numclasse==2 && player2.isSelected == NO && player2.pontdevidaatual >0){
        if ([activePlayer isKindOfClass:[PlayerMateMago class]]) {
            player1.isSelected= NO;
        }
        else if([activePlayer isKindOfClass:[PlayerHacker class]]){
            player3.isSelected=NO;
        }
        else if([activePlayer isKindOfClass:[PlayerCyberPala class]]){
            player4.isSelected=NO;
        }
        activePlayer = player2;
        player2.isSelected = YES;

    }
    else if (numclasse ==3 && player3.isSelected == NO && player3.pontdevidaatual >0){
        if ([activePlayer isKindOfClass:[PlayerEngIdol class]]) {
            player2.isSelected= NO;
        }
        else if([activePlayer isKindOfClass:[PlayerMateMago class]]){
            player1.isSelected=NO;
        }
        else if([activePlayer isKindOfClass:[PlayerCyberPala class]]){
            player4.isSelected=NO;
        }
        activePlayer = player3;
        player3.isSelected = YES;

    }
    else if (numclasse==4 && player4.isSelected == NO && player4.pontdevidaatual >0){
        if ([activePlayer isKindOfClass:[PlayerEngIdol class]]) {
            player2.isSelected= NO;
        }
        else if([activePlayer isKindOfClass:[PlayerHacker class]]){
            player3.isSelected=NO;
        }
        else if([activePlayer isKindOfClass:[PlayerMateMago class]]){
            player1.isSelected=NO;
        }
        activePlayer = player4;
        player4.isSelected = YES;
    }
    activePlayer.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:activePlayer.size];
    activePlayer.physicsBody.categoryBitMask = playerCategory;
    activePlayer.physicsBody.dynamic = NO;
    activePlayer.position = ponto;
    lock=true;
    [self.map addChild:activePlayer];
    NSLog(@"active player: %f , %f", activePlayer.position.x, activePlayer.position.y);
    [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];
    [self adicionaHP];
    SKAction *stop = [SKAction waitForDuration:0.5];
    [activePlayer runAction:stop completion:^{
        lock=false;
    }];
    
}
-(void)saveData{
//    if ([GameData sharedGameData].pecasColetadas==nil){
//        [GameData sharedGameData].pecasColetadas = [[NSMutableArray alloc]init];
//    }
    if ([GameData sharedGameData].stagesCompleted < self.scene.name.intValue ) {
        [GameData sharedGameData].stagesCompleted=self.scene.name.intValue;
        
        
    }
    NSNumber *pecasantigas;
    pecasantigas = [[GameData sharedGameData].pecasColetadas objectAtIndex:self.scene.name.intValue];
    if (pecasantigas.intValue< contador_pecas) {
        int numfase =self.scene.name.intValue;
        NSNumber *contap= [NSNumber numberWithInt:contador_pecas];
      //  NSMutableArray *teste = [GameData sharedGameData].pecasColetadas;
        [[GameData sharedGameData].pecasColetadas replaceObjectAtIndex:numfase withObject:contap ];
    }
    [[GameData sharedGameData] save];

}
-(void)autoAtaqueMago{
    Projetil * projectile = [Projetil spriteNodeWithImageNamed:@"tiromago"];
    projectile.position = player1.position;
    projectile.danoProj = 1;
    [self.map addChild:projectile];
    projectile.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:projectile.size.width/8];
    projectile.physicsBody.categoryBitMask = projectileCategory;
    projectile.physicsBody.contactTestBitMask = enemyCategory;
    projectile.physicsBody.collisionBitMask = 0;
    projectile.physicsBody.dynamic= YES;
    projectile.physicsBody.usesPreciseCollisionDetection = YES;
    [projectile runAction:magosound1];
    if ([player1 getDirection]  == 'u') {
        
        SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x , player1.position.y+ 100) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];
        NSLog(@"Uattack");
        
        
    }
    else if ([player1 getDirection]  == 'd'){
        SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x , player1.position.y- 100) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];
        NSLog(@"Dattack");
        
    }
    else if ([player1 getDirection]  == 'r'){
        SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x +100, player1.position.y) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];
        NSLog(@"Rattack");
        
    }
    else if ([player1 getDirection]  == 'l'){
        SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x -100 , player1.position.y) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"Lattack");
        
    }
    
}

-(void)autoAtaqueIdol{
    Projetil * projectile = [Projetil spriteNodeWithImageNamed:@"tiroIdol"];
    projectile.position = player2.position;
    [self.map addChild:projectile];
    projectile.danoProj = 1;
    projectile.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:projectile.size.width/8];
    projectile.physicsBody.categoryBitMask = projectileCategory;
    projectile.physicsBody.contactTestBitMask = enemyCategory;
    projectile.physicsBody.collisionBitMask = 0;
    projectile.physicsBody.dynamic= YES;
    projectile.physicsBody.usesPreciseCollisionDetection = YES;
    [projectile runAction:idolsound1];
    if ([player2 getDirection]  == 'u') {
        
        SKAction *move = [SKAction moveTo:CGPointMake(player2.position.x , player2.position.y+ 100) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"UattackIdol");
        
        
    }
    else if ([player2 getDirection]  == 'd'){
        SKAction *move = [SKAction moveTo:CGPointMake(player2.position.x , player2.position.y- 100) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"DattackIdol");
        
    }
    else if ([player2 getDirection]  == 'r'){
        SKAction *move = [SKAction moveTo:CGPointMake(player2.position.x +100, player2.position.y) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"RattackIdol");
        
    }
    else if ([player2 getDirection]  == 'l'){
        SKAction *move = [SKAction moveTo:CGPointMake(player2.position.x -100 , player2.position.y) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"LattackIdol");
        
    }

}
-(void)autoAtaqueHacker{
    Projetil  * projectile = [Projetil spriteNodeWithImageNamed:@"tiroHack"];
    projectile.position = player3.position;
    projectile.danoProj = 1;
    [self.map addChild:projectile];

    NSLog(@"Pos p3: x:%f y:%f",player3.position.x,player3.position.y);
    projectile.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:projectile.size.width/8];
    projectile.physicsBody.categoryBitMask = projectileCategory;
    projectile.physicsBody.contactTestBitMask = enemyCategory;
    projectile.physicsBody.collisionBitMask = 0;
    projectile.physicsBody.dynamic= YES;
    projectile.physicsBody.usesPreciseCollisionDetection = YES;
    
    if ([player3 getDirection]  == 'u') {
        
        SKAction *move = [SKAction moveTo:CGPointMake(player3.position.x , player3.position.y+ 100) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:hacksound1];
        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"UattackHAX");
        
        
    }
    else if ([player3 getDirection]  == 'd'){
        SKAction *move = [SKAction moveTo:CGPointMake(player3.position.x , player3.position.y- 100) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:hacksound1];

        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"DattackHAX");
        
    }
    else if ([player3 getDirection]  == 'r'){
        SKAction *move = [SKAction moveTo:CGPointMake(player3.position.x +100, player3.position.y) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:hacksound1];

        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"RattackHAX");
        
    }
    else if ([player3 getDirection]  == 'l'){
        SKAction *move = [SKAction moveTo:CGPointMake(player3.position.x -100 , player3.position.y) duration:1];
        SKAction * actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:hacksound1];

        [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
            [NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(timerColisaoProj:)
                                           userInfo:projectile
                                            repeats:YES];
            [projectile runAction:move completion:^{
                [projectile runAction:actionMoveDone];
            }];
        }]]]];                NSLog(@"LattackHAX");
        
    }

}
-(void)autoAtaquePaladino{
    Projetil * projectile = [Projetil spriteNodeWithImageNamed:@"hax"];
    //projectile.anchorPoint= CGPointMake(0, 0);
    projectile.danoProj = 1;
    projectile.position = player4.position;
    SKAction *wait = [SKAction waitForDuration:0.5];
    CGRect skill = CGRectMake(player4.position.x, player4.position.y,  player4.size.width, player4.size.height);
    projectile.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:skill.size];
    projectile.physicsBody.categoryBitMask = projectileCategory;
    projectile.physicsBody.contactTestBitMask = enemyCategory;
    projectile.physicsBody.collisionBitMask = 0;
    projectile.physicsBody.dynamic= YES;
    projectile.physicsBody.usesPreciseCollisionDetection = YES;
    [projectile runAction:palasound1];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    if ([player4 getDirection]  == 'u') {
        projectile.position = CGPointMake(player4.position.x,player4.position.y+40);
        [self.map addChild:projectile];
        [projectile runAction:[SKAction sequence:@[wait,actionMoveDone]]];
        NSLog(@"Uattack");
        
        
    }
    else if ([player4 getDirection]  == 'd'){
        projectile.position = CGPointMake(player4.position.x, player4.position.y-40);
        [self.map addChild:projectile];
        [projectile runAction:[SKAction sequence:@[wait, actionMoveDone]]];
        NSLog(@"Dattack");
        
    }
    else if ([player4 getDirection]  == 'r'){
        projectile.position = CGPointMake(player4.position.x+30, player4.position.y);
        [self.map addChild:projectile];
        [projectile runAction:[SKAction sequence:@[ wait, actionMoveDone]]];
        NSLog(@"Rattack");
        
    }
    else if ([player4 getDirection]  == 'l'){
        projectile.position = CGPointMake(player4.position.x-30, player4.position.y);
        [self.map addChild:projectile];
        [projectile runAction:[SKAction sequence:@[ wait,actionMoveDone]]];
        NSLog(@"Lattack");
    }

}

-(void) hab1Mago{
    if ([activePlayer.estadoMago1  isEqual: @"ataque"]) {
        
        Projetil * projectile = [Projetil spriteNodeWithImageNamed:@"tiro1"];
        projectile.position = player1.position;
        projectile.danoProj = 2;
        [self.map addChild:projectile];
        projectile.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:projectile.size.width/8];
        projectile.physicsBody.categoryBitMask = projectileCategory;
        projectile.physicsBody.contactTestBitMask = enemyCategory;
        projectile.physicsBody.collisionBitMask = 0;
        projectile.physicsBody.usesPreciseCollisionDetection = YES;
        SKAction *resize= [SKAction resizeByWidth:100 height:100 duration:2];
        SKAction *actionMoveDone = [SKAction removeFromParent];
        if ([player1 getDirection]  == 'u') {
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x , player1.position.y+ 100) duration:1];
            [projectile runAction:[SKAction sequence:@[move,resize, actionMoveDone]]];
            NSLog(@"UattackA");
        }
        else if ([player1 getDirection]  == 'd'){
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x , player1.position.y- 100) duration:1];
            [projectile runAction:[SKAction sequence:@[move,resize, actionMoveDone]]];
            NSLog(@"DattackA");
            
        }
        else if ([player1 getDirection]  == 'r'){
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x +100, player1.position.y) duration:1];
            [projectile runAction:[SKAction sequence:@[move,resize, actionMoveDone]]];
            NSLog(@"RattackA");
            
        }
        else if ([player1 getDirection]  == 'l'){
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x -100 , player1.position.y) duration:1];
            [projectile runAction:[SKAction sequence:@[move,resize, actionMoveDone]]];
            NSLog(@"LattackA");
            
        }
        [player1 cooldownMago1:5];
        
    }
    else NSLog(@"BOTCH");
}

-(void)hab1Idol{
    if ([player2.estadoHab1  isEqual: @"ataque"]) {
        
        SKSpriteNode * projectile = [SKSpriteNode spriteNodeWithImageNamed:@"curaIdol"];
        projectile.position = player2.position;
        projectile.zPosition = -10;
        [self.map addChild:projectile];
        SKAction *wait = [SKAction waitForDuration:5];
        SKAction *actionMoveDone = [SKAction removeFromParent];
        lock = true;
        NSTimer *curatime = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(curaTimer) userInfo:nil repeats:YES];
        SKAction *timer = [SKAction runBlock:^{
            [curatime fire];
        }];
        SKAction *timerend = [SKAction runBlock:^{
            [curatime invalidate];
        }];
        [projectile runAction:[SKAction sequence:@[timer,wait,timerend, actionMoveDone]]completion:^{
            lock=false;
        }];
        NSLog(@"Zawarudo");
        [activePlayer cooldown1:5];
    }
    else NSLog(@"BOTCH");
}
-(void)curaTimer{
    NSLog(@"Teste FIRED!");
    CGRect areacura = CGRectMake(player2.position.x, player2.position.y, 200, 200);
    if (CGRectContainsPoint(areacura, player1.position)  && player1.pontdevidaatual<player1.pontdevidamax ) {
        player1.pontdevidaatual++;
        [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [self adicionaHP];
        
    }
    if (CGRectContainsPoint(areacura, player2.position)  && player2.pontdevidaatual<player2.pontdevidamax  ) {
        player2.pontdevidaatual++;
        [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [self adicionaHP];
    }
    if (CGRectContainsPoint(areacura, player3.position)  && player3.pontdevidaatual<player3.pontdevidamax  ) {
        player3.pontdevidaatual++;
        [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [self adicionaHP];
    }
    if (CGRectContainsPoint(areacura, player4.position)   && player4.pontdevidaatual<player4.pontdevidamax ) {
        player4.pontdevidaatual++;
        [self enumerateChildNodesWithName:@"pontdevida" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [self adicionaHP];
    }
}
-(void) hab1Hacker{
    CGRect skill = CGRectMake(player3.position.x, player3.position.y, 3* player3.size.width, 3*player3.size.height);
    NSLog(@"quadradao: %f %f", skill.size.height, skill.size.width);
    //            SKSpriteNode *test = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:skill.size];
    //            test.position = activePlayer.position;
    //            [self.map addChild:test];
    for (InteractableHack *hackArray in hacks) {
        CGRect ativar = CGRectMake(hackArray.position.x, hackArray.position.y, hackArray.size.width, hackArray.size.height);
        if (CGRectIntersectsRect(skill, ativar)) {
            [hackArray removeFromParent];
            [hackArray ativarHack];
            ///Blabla animacao de hax
        }
        
        
    }
}

-(void)hab1Paladino{
    if (player4.estadoPala1 ==true && player4.estadoPala2 == true) {
        SKAction *movetimer = [SKAction runBlock:^{
            NSTimer *moveParede= [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(moveParede) userInfo:nil repeats:YES];
            
        }];
        player4.estado = @"LockDirection";
        if ([player4 getDirection]  == 'u') {
            paredePala = (Projetil *)[SKSpriteNode spriteNodeWithImageNamed:@"barreira"];
            paredePala.position = CGPointMake(player4.position.x, player4.position.y+40);
            paredePala.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:paredePala.size];
            paredePala.physicsBody.collisionBitMask=0;
            paredePala.physicsBody.categoryBitMask = PortaCategory;
            paredePala.physicsBody.contactTestBitMask= enemyProjectileCategory;
            [self.map addChild:paredePala];
            SKAction *wait = [SKAction waitForDuration:5];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [paredePala runAction:[SKAction sequence: @[movetimer,wait, actionMoveDone]]];
            NSLog(@"Uattack");
        }
        else if ([player4 getDirection]  == 'd'){
            paredePala = (Projetil *)[SKSpriteNode spriteNodeWithImageNamed:@"barreira"];
            paredePala.position = CGPointMake(player4.position.x, player4.position.y-10);
            paredePala.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:paredePala.size];
            paredePala.physicsBody.collisionBitMask=0;
            paredePala.physicsBody.categoryBitMask = PortaCategory;
            paredePala.physicsBody.contactTestBitMask= enemyProjectileCategory;
            [self.map addChild:paredePala];
            
            SKAction *wait = [SKAction waitForDuration:5];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [paredePala runAction:[SKAction sequence:@[movetimer,wait,actionMoveDone]]];
            NSLog(@"Dattack");
            
        }
        else if ([player4 getDirection]  == 'r'){
            paredePala = (Projetil *)[SKSpriteNode spriteNodeWithImageNamed:@"barreira2"];
            paredePala.position = CGPointMake(player4.position.x+30, player4.position.y);
            paredePala.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:paredePala.size];
            paredePala.physicsBody.collisionBitMask=0;
            paredePala.physicsBody.categoryBitMask = PortaCategory;
            paredePala.physicsBody.contactTestBitMask= enemyProjectileCategory;
            [self.map addChild:paredePala];
            SKAction *wait = [SKAction waitForDuration:5];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            [paredePala runAction:[SKAction sequence:@[movetimer,wait,actionMoveDone]]];
            NSLog(@"Rattack");
            
        }
        else if ([player4 getDirection]  == 'l'){
            paredePala = (Projetil *)[SKSpriteNode spriteNodeWithImageNamed:@"barreira2"];
            paredePala.position = CGPointMake(player4.position.x, player4.position.y);
            paredePala.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:paredePala.size];
            paredePala.physicsBody.collisionBitMask=0;
            paredePala.physicsBody.categoryBitMask = PortaCategory;
            paredePala.physicsBody.contactTestBitMask= enemyProjectileCategory;
            [self.map addChild:paredePala];
            
            SKAction *wait = [SKAction waitForDuration:5];
            SKAction * actionMoveDone = [SKAction removeFromParent];
            //tirei action move done
            [paredePala runAction:[SKAction sequence:@[movetimer,wait, actionMoveDone]]];
            NSLog(@"Lattack");
        }
        [player4 cooldownPala1:5];
    }

}
-(void)moveParede{
    CGPoint direction;
    switch (player4.getDirection) {
        case 'u':
            direction = CGPointMake(player4.position.x, player4.position.y+50);
            break;
        case 'd':
            direction = CGPointMake(player4.position.x, player4.position.y-10);
            break;
        case 'r':
            direction = CGPointMake(player4.position.x+30, player4.position.y);
            break;
        case 'l':
            direction = CGPointMake(player4.position.x-30, player4.position.y);
            break;
    }
    SKAction *mover = [SKAction moveTo:direction duration:0.1];
    [paredePala runAction:mover];
    
}
-(void) hab2Mago{
    if ([player1.estadoMago2  isEqual: @"ataque"]) {
        
        Projetil * projectile = [Projetil spriteNodeWithImageNamed:@"tiro1"];
        projectile.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:projectile.size];
        
        projectile.position = player1.position;
        projectile.physicsBody.categoryBitMask = projectileCategory;
        projectile.physicsBody.contactTestBitMask = enemyCategory;
        projectile.physicsBody.collisionBitMask = 0;
        projectile.physicsBody.dynamic= YES;
        projectile.physicsBody.usesPreciseCollisionDetection = YES;
        
        projectile.danoProj = 3;
        [projectile setZPosition:-10];
        SKAction *actionMoveDone = [SKAction removeFromParent];
        
        
        
        if ([player1 getDirection]  == 'u') {
            [projectile setAnchorPoint:CGPointMake(0.5, 0.4)];
            [self.map addChild:projectile];
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x , player1.position.y+ 10) duration:1];
            SKAction *resize= [SKAction resizeToHeight:1000 duration:2];
            
            [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
                
                [projectile runAction:resize completion:^{
                    [projectile runAction:actionMoveDone];
                }];
                [projectile runAction:move];
            }]]] ];
            NSLog(@"UattackA");
            
            
        }
        else if ([player1 getDirection]  == 'd'){
            [projectile setAnchorPoint:CGPointMake(0.5, 0.6)];
            [self.map addChild:projectile];
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x , player1.position.y- 10) duration:1];
            SKAction *resize= [SKAction resizeToHeight:1000 duration:2];
            
            [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
                [projectile runAction:resize completion:^{
                    [projectile runAction:actionMoveDone];
                }];
                [projectile runAction:move];
            }]]] ];
            NSLog(@"DattackA");
            
        }
        else if ([player1 getDirection]  == 'r'){
            [projectile setAnchorPoint:CGPointMake(0.4, 0.5)];
            [self.map addChild:projectile];
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x -10, player1.position.y) duration:1];
            SKAction *resize= [SKAction resizeToWidth:1000 duration:2];
            
            [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
                [projectile runAction:resize completion:^{
                    [projectile runAction:actionMoveDone];
                }];
                [projectile runAction:move];
            }]]] ];
            NSLog(@"RattackA");
            
        }
        else if ([player1 getDirection]  == 'l'){
            [projectile setAnchorPoint:CGPointMake(0.6, 0.5)];
            [self.map addChild:projectile];
            SKAction *resize= [SKAction resizeToWidth:1000 duration:2];
            
            SKAction *move = [SKAction moveTo:CGPointMake(player1.position.x -25, player1.position.y) duration:1];
            [projectile runAction:[SKAction sequence:@[[SKAction runBlock:^{
                [projectile runAction:resize completion:^{
                    [projectile runAction:actionMoveDone];
                }];
                [projectile runAction:move];
            }]]] ];
            NSLog(@"LattackA");
            
        }
        [player1 cooldownMago2:5];
    }
    

}
-(void) hab2Idol{
    if ([player2.estadoHab2  isEqual: @"ataque"]) {
        SKSpriteNode * projectile = [SKSpriteNode spriteNodeWithImageNamed:@"planet3"];
        projectile.position = player2.position;
        [self.map addChild:projectile];
        SKAction *wait = [SKAction waitForDuration:5];
        SKAction *actionMoveDone = [SKAction removeFromParent];
        [projectile runAction:[SKAction sequence:@[wait, actionMoveDone]]];
        NSLog(@"Stun");
        [player2 cooldown2:5];
    }

}
-(void) hab2Hacker{
    if ([player3.estadoHacker2 isEqual: @"ataque"]) {
        NSLog(@"HAX Invis!");
        [player3 setEstado:@"Invis"];
        SKAction *wait = [SKAction waitForDuration:3];
        SKAction *back =[SKAction fadeAlphaTo:0.5 duration:1];
        [player3 runAction:back];
        [player3 cooldownHacker2:7];
        
        [player3 runAction:wait completion:^{
            [player3 setEstado:@"pronto"];
            SKAction *tint =[SKAction fadeAlphaTo:1.0 duration:1];
            [player3 runAction:tint];
        }];
    }
    
}
-(void) hab2Paladino{
    if (player4.estadoPala1 ==true && player4.estadoPala2 == true) {
        
        [player4 setEstado:@"Investida"];
        
        
        if ([player4 getDirection]  == 'u') {
            
            SKAction *move2 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 20) duration:0.1];
            SKAction *move1 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 10) duration:0.1];
            SKAction *move3 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 30) duration:0.1];
            SKAction *move4 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 40) duration:0.1];
            SKAction *move5 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 50) duration:0.1];
            SKAction *move6 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 60) duration:0.1];
            SKAction *move7 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 70) duration:0.1];
            SKAction *move8 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 80) duration:0.1];
            SKAction *move9 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 90) duration:0.1];
            SKAction *move10 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y+ 100) duration:0.1];
            SKAction *moveall = [SKAction runBlock:^{
                [player4 runAction:move1 completion:^{
                    _desiredPosition = player4.position;
                    [player4 runAction:move2 completion:^{
                        _desiredPosition = player4.position;
                        [player4 runAction:move3 completion:^{
                            _desiredPosition = player4.position;
                            [player4 runAction:move4 completion:^{
                                _desiredPosition = player4.position;
                                [player4 runAction:move5 completion:^{
                                    _desiredPosition = player4.position;
                                    [player4 runAction:move6 completion:^{
                                        _desiredPosition = player4.position;
                                        [player4 runAction:move7 completion:^{
                                            _desiredPosition = player4.position;
                                            [player4 runAction:move8 completion:^{
                                                _desiredPosition = player4.position;
                                                [player4 runAction:move9 completion:^{
                                                    _desiredPosition = player4.position;
                                                    [player4 runAction:move10 completion:^{
                                                        _desiredPosition = player4.position;
                                                        player4.estado = @"pronto";
                                                        
                                                    }];
                                                }];
                                            }];
                                        }];
                                    }];
                                }];
                            }];
                        }];
                    }];
                }];
            }];
            
            [player4 runAction: moveall withKey:@"a"];
            
            
            
            NSLog(@"Charge U");
            
        }
        else if ([player4 getDirection]  == 'd'){
            SKAction *move2 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 20) duration:0.1];
            SKAction *move1 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 10) duration:0.1];
            SKAction *move3 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 30) duration:0.1];
            SKAction *move4 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 40) duration:0.1];
            SKAction *move5 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 50) duration:0.1];
            SKAction *move6 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 60) duration:0.1];
            SKAction *move7 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 70) duration:0.1];
            SKAction *move8 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 80) duration:0.1];
            SKAction *move9 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 90) duration:0.1];
            SKAction *move10 = [SKAction moveTo:CGPointMake(_desiredPosition.x, _desiredPosition.y- 100) duration:0.1];
            SKAction *moveall = [SKAction runBlock:^{
                [player4 runAction:move1 completion:^{
                    _desiredPosition = player4.position;
                    [player4 runAction:move2 completion:^{
                        _desiredPosition = player4.position;
                        [player4 runAction:move3 completion:^{
                            _desiredPosition = player4.position;
                            [player4 runAction:move4 completion:^{
                                _desiredPosition = player4.position;
                                [player4 runAction:move5 completion:^{
                                    _desiredPosition = player4.position;
                                    [player4 runAction:move6 completion:^{
                                        _desiredPosition = player4.position;
                                        [player4 runAction:move7 completion:^{
                                            _desiredPosition = player4.position;
                                            [player4 runAction:move8 completion:^{
                                                _desiredPosition = player4.position;
                                                [player4 runAction:move9 completion:^{
                                                    _desiredPosition = player4.position;
                                                    [player4 runAction:move10 completion:^{
                                                        _desiredPosition = player4.position;
                                                        player4.estado = @"pronto";
                                                        
                                                        
                                                    }];
                                                }];
                                            }];
                                        }];
                                    }];
                                }];
                            }];
                        }];
                        
                        
                    }];
                    
                }];
            }];
            
            [player4 runAction: moveall withKey:@"a"];
            
            
        }
        else if ([player4 getDirection]  == 'r'){
            SKAction *move2 = [SKAction moveTo:CGPointMake(_desiredPosition.x +20, _desiredPosition.y) duration:0.1];
            SKAction *move1 = [SKAction moveTo:CGPointMake(_desiredPosition.x +10, _desiredPosition.y) duration:0.1];
            SKAction *move3 = [SKAction moveTo:CGPointMake(_desiredPosition.x +30, _desiredPosition.y) duration:0.1];
            SKAction *move4 = [SKAction moveTo:CGPointMake(_desiredPosition.x +40, _desiredPosition.y) duration:0.1];
            SKAction *move5 = [SKAction moveTo:CGPointMake(_desiredPosition.x +50, _desiredPosition.y) duration:0.1];
            SKAction *move6 = [SKAction moveTo:CGPointMake(_desiredPosition.x +60, _desiredPosition.y) duration:0.1];
            SKAction *move7 = [SKAction moveTo:CGPointMake(_desiredPosition.x +70, _desiredPosition.y) duration:0.1];
            SKAction *move8 = [SKAction moveTo:CGPointMake(_desiredPosition.x +80, _desiredPosition.y) duration:0.1];
            SKAction *move9 = [SKAction moveTo:CGPointMake(_desiredPosition.x +90, _desiredPosition.y) duration:0.1];
            SKAction *move10 = [SKAction moveTo:CGPointMake(_desiredPosition.x +100, _desiredPosition.y) duration:0.1];
            SKAction *moveall = [SKAction runBlock:^{
                [player4 runAction:move1 completion:^{
                    _desiredPosition = player4.position;
                    [player4 runAction:move2 completion:^{
                        _desiredPosition = player4.position;
                        [player4 runAction:move3 completion:^{
                            _desiredPosition = player4.position;
                            [player4 runAction:move4 completion:^{
                                _desiredPosition = player4.position;
                                [player4 runAction:move5 completion:^{
                                    _desiredPosition = player4.position;
                                    [player4 runAction:move6 completion:^{
                                        _desiredPosition = player4.position;
                                        [player4 runAction:move7 completion:^{
                                            _desiredPosition = player4.position;
                                            [player4 runAction:move8 completion:^{
                                                _desiredPosition = player4.position;
                                                [player4 runAction:move9 completion:^{
                                                    _desiredPosition = player4.position;
                                                    [player4 runAction:move10 completion:^{
                                                        _desiredPosition = player4.position;
                                                        player4.estado = @"pronto";
                                                        
                                                    }];
                                                }];
                                            }];
                                        }];
                                    }];
                                }];
                            }];
                        }];
                        
                        
                    }];
                    
                }];
            }];
            
            [player4 runAction: moveall withKey:@"a"];
            NSLog(@"Charge R");
            
            
        }
        else if ([player4 getDirection]  == 'l'){
            SKAction *move2 = [SKAction moveTo:CGPointMake(_desiredPosition.x -20, _desiredPosition.y) duration:0.1];
            SKAction *move1 = [SKAction moveTo:CGPointMake(_desiredPosition.x -10, _desiredPosition.y) duration:0.1];
            SKAction *move3 = [SKAction moveTo:CGPointMake(_desiredPosition.x -30, _desiredPosition.y) duration:0.1];
            SKAction *move4 = [SKAction moveTo:CGPointMake(_desiredPosition.x -40, _desiredPosition.y) duration:0.1];
            SKAction *move5 = [SKAction moveTo:CGPointMake(_desiredPosition.x -50, _desiredPosition.y) duration:0.1];
            SKAction *move6 = [SKAction moveTo:CGPointMake(_desiredPosition.x -60, _desiredPosition.y) duration:0.1];
            SKAction *move7 = [SKAction moveTo:CGPointMake(_desiredPosition.x -70, _desiredPosition.y) duration:0.1];
            SKAction *move8 = [SKAction moveTo:CGPointMake(_desiredPosition.x -80, _desiredPosition.y) duration:0.1];
            SKAction *move9 = [SKAction moveTo:CGPointMake(_desiredPosition.x -90, _desiredPosition.y) duration:0.1];
            SKAction *move10 = [SKAction moveTo:CGPointMake(_desiredPosition.x -100, _desiredPosition.y) duration:0.1];
            SKAction *moveall = [SKAction runBlock:^{
                [player4 runAction:move1 completion:^{
                    _desiredPosition = player4.position;
                    [player4 runAction:move2 completion:^{
                        _desiredPosition = player4.position;
                        [player4 runAction:move3 completion:^{
                            _desiredPosition = player4.position;
                            [player4 runAction:move4 completion:^{
                                _desiredPosition = player4.position;
                                [player4 runAction:move5 completion:^{
                                    _desiredPosition = player4.position;
                                    [player4 runAction:move6 completion:^{
                                        _desiredPosition = player4.position;
                                        [player4 runAction:move7 completion:^{
                                            _desiredPosition = player4.position;
                                            [player4 runAction:move8 completion:^{
                                                _desiredPosition = player4.position;
                                                [player4 runAction:move9 completion:^{
                                                    _desiredPosition = player4.position;
                                                    [player4 runAction:move10 completion:^{
                                                        _desiredPosition = player4.position;
                                                        player4.estado = @"pronto";
                                                        
                                                    }];
                                                }];
                                            }];
                                        }];
                                    }];
                                }];
                            }];
                        }];
                        
                        
                    }];
                    
                }];
            }];
            
            [player4 runAction: moveall withKey:@"a"];
            NSLog(@"Charge R");
        }
        
        
        [player4 cooldownPala2];
    }
    else NSLog(@"estado: %d", player4.estadoPala2);

}
-(void)curaAll{
    player1.pontdevidaatual = player1.pontdevidamax;
    player2.pontdevidaatual = player2.pontdevidamax;
    player3.pontdevidaatual = player3.pontdevidamax;
    player4.pontdevidaatual = player4.pontdevidamax;
}
@synthesize walls,meta;


@end
