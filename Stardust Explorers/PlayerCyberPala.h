//
//  PlayerCyberPala.h
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "Player.h"

@interface PlayerCyberPala : Player{
    int hab2cooldown;
    int hab2countdown;
    NSTimer *autotime;
}

-(void)cooldownPala1:(NSTimeInterval)tempo;
-(void)cooldownPala2;

@end
