//
//  PlayerEngIdol.m
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "PlayerEngIdol.h"

@implementation PlayerEngIdol
+ (PlayerEngIdol *)defaultStore
{
    static PlayerEngIdol *defaultStore = nil;
    if(!defaultStore)
        defaultStore = [[super allocWithZone:nil] init];
    
    return defaultStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultStore];
}
-(PlayerEngIdol *) init{
    self = [super init];
    if (self){
    
        spritefiles = @"Spaceship";
        playerdirection = 'd';

        estado = @"pronto";
        estadoHab1 = @"ataque";
        estadoHab2 = @"ataque";
        pontdevidaatual = 4;
        pontdevidamax = 4;

    }
    return self;
}
-(void)cooldown1:(NSTimeInterval)tempo{
    //TRUE = não pode usar habilidade
    //FALSE = Pode usar habilidade
    estadoHab1 = @"cooldown";
    SKAction *wait =[SKAction waitForDuration:tempo];
    [self runAction:wait completion:^ {
        estadoHab1 = @"ataque";
    }];
}

-(void)cooldown2:(NSTimeInterval)tempo{
    //TRUE = não pode usar habilidade
    //FALSE = Pode usar habilidade
    estadoHab2 = @"cooldown";
    SKAction *wait =[SKAction waitForDuration:tempo];
    [self runAction:wait completion:^ {
        estadoHab2 = @"ataque";
    }];
}

@end
