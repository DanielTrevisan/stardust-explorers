//
//  MenuView.h
//  tiletest
//
//  Created by Daniel Trevisan on 21/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
@class MutiViewController;

@interface MenuView : SKScene{
    SKSpriteNode *btnStart;
    SKSpriteNode *btnMultiStart;
    SKSpriteNode *btnOptions;

}
@property (nonatomic, weak) MutiViewController *spriteViewController;

@end
