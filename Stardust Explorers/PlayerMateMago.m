//
//  PlayerMateMago.m
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "PlayerMateMago.h"

@implementation PlayerMateMago
+ (PlayerMateMago *)defaultStore
{
    static PlayerMateMago *defaultStore = nil;
    if(!defaultStore)
        defaultStore = [[super allocWithZone:nil] init];
    
    return defaultStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultStore];
}
-(PlayerMateMago *) init{
    self = [super init];
    if (self){
        spritefiles= @"koalio_stand.png";
        playerdirection = 'd';

        estado = @"pronto";
        estadoMago1 = @"ataque";
        estadoMago2 = @"ataque";
        pontdevidaatual = 3;
        pontdevidamax = 3;
    }
    return self;
}

-(void)cooldownMago1:(NSTimeInterval)tempo{
    estadoMago1 = @"cooldown";
    SKAction *wait = [SKAction waitForDuration:tempo];
    [self runAction:wait completion:^{
        estadoMago1 = @"ataque";
    }];
}

-(void)cooldownMago2:(NSTimeInterval)tempo{
    estadoMago2 = @"cooldown";
    SKAction *wait = [SKAction waitForDuration:tempo];
    [self runAction:wait completion:^{
        estadoMago2 = @"ataque";
    }];
}

@end
