//
//  World1f3.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 08/12/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "WorldBase.h"

@interface World1f3 : WorldBase
@property (nonatomic, strong) JSTileMap *map;
@property (strong) TMXLayer *meta;
@property (nonatomic, strong) TMXLayer *walls;
@property (nonatomic, assign) CGPoint desiredPosition;

@end
