//
//  GameData.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 29/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "GameData.h"
#import "KeychainWrapper.h"

static NSString* const SSGameDataStagesCompleted = @"stagesCompleted";
static NSString* const SSGameDataPecasColetadas = @"pecasColetadas";
static NSString* const SSGameDataHighScore = @"highScore";
static NSString* const SSGameDataVolMusic = @"volMusic";
static NSString* const SSGameDataVolSFX = @"volSFX";
static NSString* const SSGameDataChecksumKey = @"SSGameDataChecksumKey";
static NSString* const SSGameDataLastStage = @"UltimoStage";
static NSString* const SSGameDataMultiStage = @"MultiStage";
static NSString* const SSGameDataNumPecas = @"NumPecas";



@implementation GameData
- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        _stagesCompleted = [decoder decodeIntegerForKey: SSGameDataStagesCompleted];
        _pecasColetadas = [decoder decodeObjectForKey: SSGameDataPecasColetadas];
        highScore = [decoder decodeObjectForKey:SSGameDataHighScore];
        _volMusic = [decoder decodeIntegerForKey:SSGameDataVolMusic];
        _volSFX = [decoder decodeIntegerForKey:SSGameDataVolSFX];
        _ultimoStage = [decoder decodeIntegerForKey:SSGameDataLastStage];
        _stageMult = [decoder decodeIntegerForKey:SSGameDataMultiStage];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInteger:_stagesCompleted forKey:SSGameDataStagesCompleted];
    [encoder encodeInteger:_volMusic forKey:SSGameDataVolMusic];
    [encoder encodeInteger:_volSFX forKey:SSGameDataVolSFX];
    [encoder encodeObject:_pecasColetadas forKey:SSGameDataPecasColetadas];
    [encoder encodeObject:highScore forKey:SSGameDataHighScore];
    [encoder encodeInteger:_ultimoStage forKey:SSGameDataLastStage];
    [encoder encodeInteger:_stageMult forKey:SSGameDataMultiStage];

}
+ (GameData *)sharedGameData {
  // static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    static GameData *gmdta = nil;
    dispatch_once(&onceToken, ^{
        gmdta = [self loadInstance];
       // NSMutableArray *teste = gmdta.pecasColetadas;
        if ([gmdta.pecasColetadas count]==0){
            gmdta.pecasColetadas = [[NSMutableArray alloc]initWithCapacity:48];
            for (int i=1; i<=48; i++) {
            [gmdta.pecasColetadas addObject:@(0)];
            
            }
        }
    });
    return gmdta;
}
-(void)resetData{
    _stagesCompleted = 0;
    [_pecasColetadas removeAllObjects];
    [highScore removeAllObjects];
    _volMusic = 100;
    _volSFX =   100;
    _ultimoStage = 0;
    _stageMult = 0;
}
+(NSString*)filePath
{
    static NSString* filePath = nil;
    if (!filePath) {
        filePath =
        [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
         stringByAppendingPathComponent:@"gamedata"];
    }
    return filePath;
}
+(GameData *)loadInstance
{
    NSData* decodedData = [NSData dataWithContentsOfFile: [GameData filePath]];
    if (decodedData) {
        //1
        NSString* checksumOfSavedFile = [KeychainWrapper computeSHA256DigestForData: decodedData];
        
        //2
        NSString* checksumInKeychain = [KeychainWrapper keychainStringFromMatchingIdentifier: SSGameDataChecksumKey];
        
        //3
        if ([checksumOfSavedFile isEqualToString: checksumInKeychain]) {
            GameData* gameData = [NSKeyedUnarchiver unarchiveObjectWithData:decodedData];
            return gameData;
        }
        //4
    }
    
    return [[GameData alloc] init];
}
-(void)save
{
    NSData* encodedData = [NSKeyedArchiver archivedDataWithRootObject: self];
    [encodedData writeToFile:[GameData filePath] atomically:YES];
    NSString* checksum = [KeychainWrapper computeSHA256DigestForData: encodedData];
    if ([KeychainWrapper keychainStringFromMatchingIdentifier: SSGameDataChecksumKey]) {
        [KeychainWrapper updateKeychainValue:checksum forIdentifier:SSGameDataChecksumKey];
    } else {
        [KeychainWrapper createKeychainValue:checksum forIdentifier:SSGameDataChecksumKey];
    }

}
@synthesize highScore;
@end
