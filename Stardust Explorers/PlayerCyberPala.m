//
//  PlayerCyberPala.m
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "PlayerCyberPala.h"

@implementation PlayerCyberPala
+ (PlayerCyberPala *)defaultStore
{
    static PlayerCyberPala *defaultStore = nil;
    if(!defaultStore)
        defaultStore = [[super allocWithZone:nil] init];
    
    return defaultStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultStore];
}
-(PlayerCyberPala *) init{
    self = [super init];
    if (self){
        spritefiles = @"koalio_stand";
        playerdirection = 'd';
        estado = @"pronto";
        pontdevidaatual = 5;
        pontdevidamax = 5;
        estadoPala1 = YES;
        estadoPala2 = YES;
        hab2cooldown = 2;
        hab2countdown = 0;

    }
    return self;
}
-(void)cooldownPala1:(NSTimeInterval)tempo{
    //TRUE = não pode usar habilidade
    //FALSE = Pode usar habilidade
    estadoPala1 = NO;
    SKAction *wait =[SKAction waitForDuration:tempo];
        [self runAction:wait completion:^ {
            self.estado=@"pronto";
        estadoPala1 = YES;
    }];
}

-(void)setEstadoHabPala2{
    if (hab2countdown >=hab2cooldown) {
        estadoPala2=YES;
        [autotime invalidate];
        hab2countdown = 0;
    }
    else{
        hab2countdown++;
    }
    
}

-(void)cooldownPala2{
    //TRUE = não pode usar habilidade
    //FALSE = Pode usar habilidade
    estadoPala2 = NO;
    // SKAction *wait =[SKAction waitForDuration:tempo];
    autotime = [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(setEstadoHabPala2)
                                   userInfo:nil
                                    repeats:YES];
//    estadoPala2 = NO;
//    SKAction *wait =[SKAction waitForDuration:tempo];
//    [self runAction:wait completion:^ {
//        estadoPala2 = YES;
//    }];
}



@end
