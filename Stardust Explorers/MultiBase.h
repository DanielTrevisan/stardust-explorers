//
//  MultiBase.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 07/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameKitHelper.h"

typedef NS_ENUM(NSUInteger, ClassName) {
    kClassIdol,
    kClassPaladin,
    kClassHacker,
    kClassMage
};

@interface MultiBase : SKScene{
    SKSpriteNode *selectIdol;
    SKSpriteNode *selectHack;
    SKSpriteNode *selectPala;
    SKSpriteNode *selectMago;
    BOOL idolSelected;
    BOOL palaSelected;
    BOOL magoSelected;
    BOOL hackSelected;
    BOOL matchStarted;
    SKLabelNode *start;
    NSMutableArray *stagesPlayers;
    NSMutableArray *outrosVotos;
}
@property ClassName ultimaclasse;
@property (nonatomic, strong) GKMatch *matchBase;
@property (nonatomic,strong) GKMatchRequest *request;
+ (instancetype)sharedGameKitHelper;
- (void)sendData:(NSData*)data;
- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID;


@end

