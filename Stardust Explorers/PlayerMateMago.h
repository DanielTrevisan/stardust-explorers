//
//  PlayerMateMago.h
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "Player.h"

@interface PlayerMateMago : Player
-(void)cooldownMago1:(NSTimeInterval)tempo;
-(void)cooldownMago2:(NSTimeInterval)tempo;

@end
