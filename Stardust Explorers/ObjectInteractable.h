//
//  ObjectInteractable.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 03/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ObjectInteractable : SKSpriteNode

@end
