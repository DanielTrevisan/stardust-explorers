//
//  GameKitHelper.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 06/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

@import GameKit;
@protocol GameKitHelperDelegate
- (void)matchStarted;
- (void)matchEnded;
- (void)match:(GKMatch *)match didReceiveData:(NSData *)data
   fromPlayer:(NSString *)playerID;
@end
@interface GameKitHelper : NSObject <GKMatchmakerViewControllerDelegate, GKMatchDelegate>

@property (nonatomic, strong) GKMatch *match;
@property (nonatomic, assign) id <GameKitHelperDelegate> delegate;

- (void)findMatchWithMinPlayers:(int)minPlayers maxPlayers:(int)maxPlayers
                 viewController:(UIViewController *)viewController
                       delegate:(id<GameKitHelperDelegate>)delegate;
@property (nonatomic, readonly) UIViewController *authenticationViewController;
@property (nonatomic) BOOL enableGameCenter;
@property (nonatomic, readonly) NSError *lastError;
+ (instancetype)sharedGameKitHelper;
- (void)authenticateLocalPlayer;
extern NSString *const PresentAuthenticationViewController;


@end
