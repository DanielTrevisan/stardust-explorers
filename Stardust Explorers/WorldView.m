//
//  WorldView.m
//  tiletest
//
//  Created by Daniel Trevisan on 21/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "WorldView.h"
#import "World1.h"
#import "WorldTeste.h"
#import "SelecaoDeFases1.h"

@implementation WorldView
-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:@"space1.jpg"];
        bgImage.position = CGPointMake(self.size.width/2,self.size.height/2);
        [self addChild:bgImage];
        self.view.backgroundColor = [UIColor whiteColor];
        World1pla = [SKSpriteNode spriteNodeWithImageNamed:@"Office_Planet"];
        World1pla.position = CGPointMake(self.size.width*1/8, (self.size.height*7/8)-30);
        [self addChild:World1pla];
        SKLabelNode *w1= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w1.text = @"Planeta Escritório";
        w1.position = CGPointMake(self.size.width*1/8 + World1pla.size.width,(self.size.height*7/8)-30);
        [self addChild:w1];
        
        World2 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World2.position = CGPointMake(self.size.width*1/8, self.size.height*5/8);
        
        [self addChild:World2];
        
        SKLabelNode *w2= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w2.text = @"Planeta Circassino";
        w2.fontColor = [UIColor grayColor];
        w2.position = CGPointMake(self.size.width*1/4 + 2* World2.size.width, self.size.height*5/8);
        [self addChild:w2];
        
        World3 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World3.position = CGPointMake(self.size.width*1/8, self.size.height*3/8);
        [self addChild:World3];
        SKLabelNode *w3= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w3.text = @"Planeta Planta";
        w3.fontColor = [UIColor grayColor];

        w3.position = CGPointMake(self.size.width*1/4 + 2* World2.size.width, self.size.height*3/8);
        [self addChild:w3];
        
        World4 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World4.position = CGPointMake(self.size.width*1/8, self.size.height*1/8);
        [self addChild:World4];
        SKLabelNode *w4= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w4.text = @"Planeta Ninja-Pirata";
        w4.fontColor = [UIColor grayColor];

        w4.position = CGPointMake(self.size.width*1/4 + 2* World2.size.width, self.size.height*1/8);
        [self addChild:w4];
        
        World5 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World5.position = CGPointMake((self.size.width*2/3)-30, self.size.height*7/8);
        [self addChild:World5];
        SKLabelNode *w5= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w5.text = @"Planeta Assombrado";
        w5.fontColor = [UIColor grayColor];

        w5.position = CGPointMake((self.size.width*2/3 + 2* World2.size.width)+30, self.size.height*7/8);
        [self addChild:w5];
        
        World6 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World6.position = CGPointMake((self.size.width*2/3)-30, self.size.height*5/8);
        [self addChild:World6];
        SKLabelNode *w6= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w6.text = @"Planeta Pintura";
        w6.fontColor = [UIColor grayColor];
        w6.position = CGPointMake(self.size.width*2/3 + 2* World2.size.width, self.size.height*5/8);
        [self addChild:w6];
        
        World7 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World7.position = CGPointMake((self.size.width*2/3)-30, self.size.height*3/8);
        [self addChild:World7];
        SKLabelNode *w7= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w7.text = @"Planeta Azul";
        w7.fontColor = [UIColor grayColor];

        w7.position = CGPointMake(self.size.width*2/3 + 2* World2.size.width, self.size.height*3/8);
        [self addChild:w7];
        
        World8 = [SKSpriteNode spriteNodeWithImageNamed:@"joystick.png"];
        World8.position = CGPointMake((self.size.width*2/3)-30, self.size.height*1/8);
        [self addChild:World8];
        SKLabelNode *w8= [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        w8.text = @"Planeta Robô";
        w8.fontColor = [UIColor grayColor];

        w8.position = CGPointMake(self.size.width*2/3 + 2* World2.size.width, self.size.height*1/8);
        [self addChild:w8];
        
        
        
        
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        CGPoint location = [touch locationInNode:self];
        
        if ([World1pla containsPoint:location])
        {
            SKView * skView = (SKView *)self.view;
            SKScene *scene = [SelecaoDeFases1 sceneWithSize:skView.bounds.size];
            
            SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
            [self.view presentScene:scene transition:sceneTransition];
            NSLog(@"Mundo 1");

            
        }
        if ([World2 containsPoint:location])
        {
            NSLog(@"Mundo 2");
            
            
        }
        if ([World3 containsPoint:location])
        {
            NSLog(@"Mundo 3");
        }
        if ([World4 containsPoint:location])
        {
            NSLog(@"Mundo 4");
        }
        if ([World5 containsPoint:location])
        {
            NSLog(@"Mundo 5");
        }
        if ([World6 containsPoint:location])
        {
            NSLog(@"Mundo 6");
        }
        if ([World7 containsPoint:location])
        {
            NSLog(@"Mundo 7");
        }
        if ([World8 containsPoint:location])
        {
            NSLog(@"Mundo 8");
        }
            
            
        
    }
}
@end
