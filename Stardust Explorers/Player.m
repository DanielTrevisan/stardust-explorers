//
//  Player.m
//  tiletest
//
//  Created by Daniel Trevisan on 20/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "Player.h"

@implementation Player
-(NSString *)getSpriteAtlas{
    return spritefiles;
}
-(char)getDirection{
    return playerdirection;
    
}
-(void)setDirection:(char) direction{
    playerdirection = direction;
}

-(NSString *)getEstado{
    return estado;
}
-(int)getHitPoints{
    NSLog(@"%d", pontdevidaatual);
    return pontdevidaatual;
}

@synthesize estadoHab1;
@synthesize estadoHab2;
@synthesize estadoMago1;
@synthesize estadoMago2;
@synthesize estadoPala1;
@synthesize estadoPala2;
@synthesize estadoHacker1;
@synthesize estadoHacker2;
@synthesize pontdevidaatual;
@synthesize pontdevidamax;
@synthesize estado;
@end
