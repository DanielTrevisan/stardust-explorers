//
//  Projetil.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 10/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Projetil : SKSpriteNode{
    int danoProj;
    NSString *tipoProj;
}

@property int danoProj;
@property NSString *tipoProj;
@end
