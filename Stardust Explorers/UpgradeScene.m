//
//  UpgradeScene.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 04/12/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "UpgradeScene.h"
#import "GameData.h"
#import "SelecaoDeFases1.h"

@implementation UpgradeScene
-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        SKLabelNode *parabens = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        parabens.text = @"Pecas coletadas!";
        int nummaxpecas = 0;
        for (NSNumber *numpeca in [GameData sharedGameData].pecasColetadas) {
            nummaxpecas += numpeca.integerValue;
        }
        NSString *pecasText = [NSString stringWithFormat:@"Pecas coletadas: %d",nummaxpecas];
        parabens.text= pecasText;
        parabens.fontSize = 50;
        parabens.position = CGPointMake(self.size.width*1/2, self.size.height*5/6);
        [self addChild:parabens];
        
        back = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        back.text = @"Voltar à seleção de fases";
        back.position = CGPointMake(self.size.width/2, self.size.height*1/8);
        [self addChild:back];
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        
        CGPoint location = [touch locationInNode:self];
            if([back containsPoint:location]){
                SKView * skView = (SKView *)self.view;
                SKScene *scene = [SelecaoDeFases1 sceneWithSize:skView.bounds.size];
                SKTransition *sceneTransition = [SKTransition fadeWithColor:[UIColor darkGrayColor] duration:1];
                [self.view presentScene:scene transition:sceneTransition];
        }
        
    }
}
@end
