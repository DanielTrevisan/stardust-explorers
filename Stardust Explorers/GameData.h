//
//  GameData.h
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 29/10/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>

@interface GameData : NSObject <NSCoding>


@property (assign, nonatomic) int stagesCompleted;
@property (nonatomic,retain)NSMutableArray *pecasColetadas;
@property (nonatomic,retain)NSMutableArray *highScore;
//colocar algo de upgrades
@property (assign, nonatomic) int volMusic;
@property (assign, nonatomic) int volSFX;
@property (assign, nonatomic) int ultimoStage;
@property (assign, nonatomic) int stageMult;
@property (assign,nonatomic) int numPecas;

-(void)save;

+(instancetype)sharedGameData;
-(void)resetData;

@end