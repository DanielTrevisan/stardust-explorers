//
//  WorldTeste.m
//  Stardust Explorers
//
//  Created by Daniel Trevisan on 08/09/14.
//  Copyright (c) 2014 Yarasoft. All rights reserved.
//

#import "WorldTeste.h"

@implementation WorldTeste
-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self removeAllChildren];
        [self removeAllActions];
        self.physicsWorld.contactDelegate = self;
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.backgroundColor = [SKColor colorWithRed:0 green:0 blue:0 alpha:1.0];
        self.scaleMode = SKSceneScaleModeAspectFill;
        self.map = [JSTileMap mapNamed:@"Fase_2.tmx"];
        walls = [self.map layerNamed:@"Meta"];
        walls.layerInfo.visible = false;
        self.scene.name= @"2";
        [super addChild:self.map];
        [super addPlayers];
        [super addEnemies: 6 andDrones:1];
        [super addObstaculos:1 andWalls:2];
        [self addpecas];
        [self adicionaHP];
        
        //Joystick
        SKSpriteNode *jsThumb = [SKSpriteNode spriteNodeWithImageNamed:@"joystick"];
        SKSpriteNode *jsBackdrop = [SKSpriteNode spriteNodeWithImageNamed:@"dpad"];
        joystick = [Joystick joystickWithThumb:jsThumb andBackdrop:jsBackdrop];
        joystick.position = CGPointMake(100, 100);
        [self addChild:joystick];
        
        velocityTick = [CADisplayLink displayLinkWithTarget:self selector:@selector(joystickMovement)];
        [velocityTick addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        //Botoes
        [self addBotoes];
        [self addCharaChange];
        [NSTimer scheduledTimerWithTimeInterval:0.5
                                         target:self
                                       selector:@selector(attackEnemies)
                                       userInfo:nil
                                        repeats:YES];
        
    }
    return self;
}
-(void)joystickMovement
{
    if ((joystick.velocity.x != 0 || joystick.velocity.y != 0)&& lock== false)
    {
        super.desiredPosition = CGPointMake(activePlayer.position.x + .05 *joystick.velocity.x, activePlayer.position.y + .05 * joystick.velocity.y);
    }
    if (joystick.velocity.x > 0 && ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'r'];
    }
    else if (joystick.velocity.x < 0 && ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'l'];
    }
    if (joystick.velocity.y > 0 && abs(joystick.velocity.y)>abs(joystick.velocity.x) && ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'u'];
    }
    else if (joystick.velocity.y < 0 && abs(joystick.velocity.y)>abs(joystick.velocity.x)&& ![activePlayer.estado isEqual:@"LockDirection"])
    {
        [activePlayer setDirection:'d'];
    }
}
-(void)timerColisaoProj:(NSTimer *)timer{
    Projetil *proj = [timer userInfo];
    
    [self checkForAndResolveCollisionsforLayer:walls andProjectile:proj];
}
-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    [self setViewpointCenter:super.desiredPosition];
    [self checkForAndResolveCollisionsforLayer:walls];
    [self teleporters];
    if(super.finish == NO){
        [self terminarfase];
    }
    
    
}
@synthesize walls,meta;

@end
