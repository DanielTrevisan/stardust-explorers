//
//  AppDelegate.h
//  tiletest
//
//  Created by Daniel Trevisan on 18/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
