//
//  ViewController.m
//  tiletest
//
//  Created by Daniel Trevisan on 18/08/14.
//  Copyright (c) 2014 super. All rights reserved.
//

#import "ViewController.h"
#import "World1.h"
#import "MenuView.h"
#import "GameKitHelper.h"


// Mark the GameViewController to implement GameKitHelperDelegate
@interface ViewController()<GameKitHelperDelegate, GKGameCenterControllerDelegate>

@end

@implementation ViewController 

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    skView.showsPhysics = NO;
    // Create and configure the scene.
    SKScene * scene = [MenuView sceneWithSize:skView.bounds.size];
    scene.backgroundColor = [UIColor darkGrayColor];
    scene.scaleMode = SKSceneScaleModeAspectFill;    
    // Present the scene.
    [skView presentScene:scene];
}


- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark GameKitHelperDelegate

- (void)matchStarted {
    NSLog(@"Match started");
}

- (void)matchEnded {
    NSLog(@"Match ended");
}

- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID {
    NSLog(@"Received data");
}
@end
